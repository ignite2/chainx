# Chain X Graphics Engine

_A graphics engine in the making and my base of research for the ins and outs of computer graphics_

So far in my early career, I have focused on various data-processing related projects and data visualization. I enjoy both of these domains and will use my experience as I delve into the world of graphics engines.   

The end goal of this project is to develop an application to visualize mesh models, architecture, particle effects, and run physics-based simulations.

This graphics engine is early in development, and currently uses modern shader-based OpenGL. Eventually, a transition to Vulkan will be made after implementing/learning a few more graphics concepts. In its present form, this engine is essentially a foundation for new, more specialized features in the future.   

![](assets/screenshots/Screenshot_sculpture.jpg)

## Build / Dev Environment

##### Linux

In the tools folder, there is a tools_inst.sh script to install required packages + compilers to build this project on Linux.    
`./tools_inst.sh`

##### Windows

Easiest way to get the compilers and dev kit is to use the Visual Studio Community Edition installer and make sure to also install the Windows 10 SDK.   

### Dependencies

Qt and Assimp need to be installed manually before building Chain X.

- Qt v5.15 (<https://www.qt.io/download>)
- Assimp v5.0.1 (<https://github.com/assimp/assimp>)

## Features (Pre-alpha)

- C++17 Standard
- Built on the Qt framework
- Model loading with Assimp library
- Component-based Entity System (Not fully complete)

## TODO List

<sup>This list is not strictly ordered, but in general, items that I want to implement sooner rather than later are near the top. This list is also subject to frequent updates.   </sup>

- Implement Camera pick raycasting functionality
- Add renderable Transform handle
- Update color palette for GUI
- Update Point Light implementation to use a radius attenuation model
- Add GUIs for light components
- Add GUI options for mouse sensitivity + render options
- Update Camera sensitivity options for movement/rotation/zoom
- Convert qmake to cmake
- Add custom Shader Loader with pre-processor
- Add Threading Library
- Add Blending render pass
- Update Post-processing render pass
- Add Blur effects
- Add new GUI tabs for different render canvases
- Add Edit Mesh mode canvas
- Add Simulation capabilities to Engine Context
- Add Physics Engine
