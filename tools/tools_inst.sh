
apt-get install -y \
	wget tar xz-utils make g++ gdb clang \
	autoconf automake libtool libbz2-dev \
	libglew-dev libgtk2.0-dev libgtk-3-dev \
	libxkbcommon-x11-dev libxtst-dev \
	libx11-xcb-dev libxcb-xfixes0-dev libxcb-dri2-0-dev \
	libxcb-xinerama0
