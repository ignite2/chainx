#include "glad/glad.h"
#include "sandbox.h"
#include "mesh_shapes.h"

#include <chrono>

#include "Mesh.h"
#include "Material.h"
#include "ShaderLibrary.h"
#include "Texture.h"

#include "Entity.h"
#include "RenderModel.h"
#include "Transform.h"
#include "DirectionalLight.h"
#include "PointLight.h"

// TODO: Make resource class system instead of using Qt
#include <filesystem>

namespace ChainX
{
namespace
{
std::shared_ptr<Entity> CreateMeshEntity(const std::string& name,
                                         std::shared_ptr<Mesh> mesh,
                                         const std::shared_ptr<QOpenGLShaderProgram>& shader,
                                         Scene& scene)
{
    mesh->Finalize(shader.get());

    std::shared_ptr<Material> material(new Material(shader));

    std::shared_ptr<Entity> mesh_ent = scene.CreateEntity();
    mesh_ent->SetName(name);
    mesh_ent->SetRenderModel(std::make_shared<RenderModel>(mesh, material, mesh_ent.get()));
    return mesh_ent;
}

std::shared_ptr<Entity> CreatePointLightEntity(const std::string& name,
                                               const QVector3D& position,
                                               const ShaderLibrary& shaderLib,
                                               Scene& scene)
{
    auto light_shader = shaderLib.GetShader(ShaderType::GPU_UNIFORM_COLOR);
    std::shared_ptr<Mesh> mesh = utils::shapes::CreateCube();
    mesh->Finalize(light_shader.get());
    std::shared_ptr<Material> material(new Material(light_shader, ShaderType::GPU_UNIFORM_COLOR));
    material->SetBaseColor( QVector3D(1.f, 1.f, 1.f) );

    std::shared_ptr<Entity> light_cube = scene.CreateEntity();
    light_cube->SetName(name);
    light_cube->SetRenderModel(std::make_shared<RenderModel>(mesh, material,light_cube.get()));
    light_cube->GetTransform()->SetPosition(position);
    light_cube->GetTransform()->SetScale({0.2f, 0.2f, 0.2f});
    light_cube->SetPointLight(CreatePointLight(QVector4D{0.8f, 0.8f, 0.8f, 1.f}, position));

    return light_cube;
}
}

namespace sandbox
{
void Test(Scene& scene, const ShaderLibrary& shaderLib)
{
    QVector3D orange_color{1.f, 0.5f, 0.31f};

    auto shader = shaderLib.GetShader(ShaderType::GPU_DEFAULT);

    auto cylinder =
            CreateMeshEntity("Cylinder", utils::shapes::CreateCylinder(), shader, scene);
         cylinder->GetRenderModel()->GetMaterial()->SetBaseColor(orange_color);

    auto cube2 = CreateMeshEntity("Cube2", utils::shapes::CreateCube(), shader, scene);
         cube2->GetTransform()->SetPosition({ 2., 0., -2.5});

    auto cube3 = CreateMeshEntity("Cube3", utils::shapes::CreateCube(), shader, scene);
         cube3->GetTransform()->SetPosition({-2., 0., -2.5});

    auto cube4 = CreateMeshEntity("Cube2_c1", utils::shapes::CreateCube(), shader, scene);
         cube2->AddChild(cube4);
         cube2->GetTransform()->SetChildren(cube2->GetChildren());
         cube4->GetTransform()->SetPosition({.25, 1., 0.});

    // Directional light entity
    auto light_entity = scene.CreateEntity();
    light_entity->SetDirLight(CreateDirLight({ -.4f, -1.f, -.3f, 0.f }));
    light_entity->SetName("Dir light 1");

    // Point light entities
    auto pt_light1 = CreatePointLightEntity("PointLight1", QVector3D{-1.2f, 1.2f, 2.0f},
                                            shaderLib, scene);

    auto pt_light2 = CreatePointLightEntity("PointLight2", QVector3D{1.2f, -.5f, 2.0f},
                                            shaderLib, scene);
}

} // end sandbox namespace
}







