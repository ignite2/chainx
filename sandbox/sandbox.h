#ifndef SANDBOX_H
#define SANDBOX_H

#include "ShaderLibrary.h"
#include "Scene.h"

class Material;

namespace ChainX
{
namespace sandbox
{
void Test(Scene& scene, const ShaderLibrary& shaderLib);

}
}

#endif // SANDBOX_H
