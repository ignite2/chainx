#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_MainWindow.h"
#include <QMainWindow>

class QLabel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

    void OnStartup();

public slots:
    void UpdateStatusBar(const std::shared_ptr<ChainX::Entity>& entity);

protected slots:
    void LoadModelFile();
    void ShowAbout();

private:
    Ui::MainWindow ui;
    QLabel* mStatusLabel;

};

#endif // MAINWINDOW_H
