#pragma once

#include <QOpenGLWidget>

#include "Renderer.h"
#include "Scene.h"
#include "Camera.h"

class EditMaterialWidget;

class QGLCanvas : public QOpenGLWidget
{
    Q_OBJECT

public:
    QGLCanvas(QWidget* parent = nullptr);
    ~QGLCanvas();

    void SetCanvasScene(std::shared_ptr<ChainX::Scene> scene);

    ChainX::Renderer* Renderer() const { return mRenderer.get(); };
    ChainX::Scene* Scene() const { return mScene.get(); };

public slots:
    void SelectEntity(const std::shared_ptr<ChainX::Entity>& entity);
    void ScanScene();

signals:
    void EntitySelected(const std::shared_ptr<ChainX::Entity>& entity);

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void keyPressEvent(QKeyEvent* e);
    void keyReleaseEvent(QKeyEvent* e);
    void mouseMoveEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void wheelEvent(QWheelEvent* e);

protected slots:

private:
    float mLastX;
    float mLastY;
    bool  mMouseIsMoving;

    ChainX::Camera mCamera;
    std::shared_ptr<ChainX::Renderer> mRenderer;
    std::shared_ptr<ChainX::Scene> mScene;
};

