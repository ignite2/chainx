#ifndef SCENETREEWIDGET_H
#define SCENETREEWIDGET_H

#include <QTreeWidget>
#include <QWidget>

namespace ChainX
{
class Entity;
class Scene;
}
class QTreeWidgetItem;

class SceneTreeWidget : public QWidget
{
    Q_OBJECT

public:

    explicit SceneTreeWidget(QWidget* parent = nullptr);

    void SetScene(ChainX::Scene* scene);

signals:

    void EntitySelected(const std::shared_ptr<ChainX::Entity>& entity);
    void SceneChanged();

private slots:

    void OnItemSelected();
    void OnCtxMenu(const QPoint& pos);
    void OnDeleteAction();

private:
    void keyPressEvent(QKeyEvent* e);

    void DeleteSceneItem(QTreeWidgetItem* item);

    std::unique_ptr<QTreeWidget> mSceneTree;
    ChainX::Scene* mScene;
};

#endif // SCENETREEWIDGET_H
