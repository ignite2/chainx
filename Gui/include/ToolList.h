#ifndef TOOLLIST_H
#define TOOLLIST_H


#include <QLabel>
#include <QWidget>


class QVBoxLayout;

//////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief ToolLabel class that is just a clickable QLabel
 */
class ToolLabel : public QLabel
{
    Q_OBJECT
public:
    ToolLabel(const QString& title);
signals:
    void clicked();
protected:
    void mousePressEvent(QMouseEvent* event);
};

//////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief ToolItem class that holds a clickable Tool Label
 * and item widget
 */
class ToolItem : public QWidget
{
    Q_OBJECT

    std::unique_ptr<ToolLabel> mLabel;
    std::unique_ptr<QWidget>   mItem;
public:
    ToolItem(const QString& title, QWidget* item);
protected slots:
    void OnLabelClicked();
};

//////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief ToolList class to hold a list of Tool Items that
 * can be expanded and collapsed.
 */
class ToolList : public QWidget
{
    Q_OBJECT

public:

    ToolList(QWidget* parent = nullptr);
    ~ToolList();

    /**
     * @brief Adds tool item to list. Takes ownership of item pointer
     */
    void AddItem(ToolItem* item);

    void Clear();

protected:

    void paintEvent(QPaintEvent* event);

private:
    QVBoxLayout* mLayout;
};

#endif // TOOLLIST_H
