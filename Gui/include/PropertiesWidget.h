#ifndef PROPERTIESWIDGET_H
#define PROPERTIESWIDGET_H

#include <QWidget>
#include <QScrollArea>
#include <memory>

namespace ChainX
{
class Entity;
}
class ToolList;

class PropertiesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PropertiesWidget(QWidget* parent = nullptr);

public slots:
    void SetEntity(const std::shared_ptr<ChainX::Entity>& entity);

signals:
    void PropertiesChanged();

private:
    // Scroll List to hold Property components/widgets
    std::unique_ptr<QScrollArea> mScrollList;
};

#endif // PROPERTIESWIDGET_H
