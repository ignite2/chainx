#ifndef EDITTRANSFORMWIDGET_H
#define EDITTRANSFORMWIDGET_H

#include <QWidget>

namespace Ui
{
class EditTransformWidget;
}
namespace ChainX
{
class Transform;
}

class EditTransformWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EditTransformWidget(QWidget* parent = nullptr);
    ~EditTransformWidget();

    void SetTransform(ChainX::Transform* transform);

signals:
    void TransformChanged();

protected slots:
    void OnPosUpdate();
    void OnRotUpdate();
    void OnScaleUpdate();

private:
    Ui::EditTransformWidget *ui;

    ChainX::Transform* mTransform;
};

#endif // EDITTRANSFORMWIDGET_H
