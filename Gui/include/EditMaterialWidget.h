#ifndef EDITMATERIALWIDGET_H
#define EDITMATERIALWIDGET_H

#include "Material.h"

#include <QWidget>

namespace Ui
{
class EditMaterialWidget;
}

class EditMaterialWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EditMaterialWidget(QWidget* parent = nullptr);
    ~EditMaterialWidget();

    void SetMaterial(ChainX::Material* material);

signals:
    void MaterialChanged();

protected slots:
    void OnChangeBaseColor();
    void OnChangeSpecColor();
    void OnBaseColorChanged(const QColor& color);
    void OnSpecColorChanged(const QColor& color);
    void OnSpecPowerChanged(double value);
    void OnSpecStrengthChanged(double value);

private:
    Ui::EditMaterialWidget* ui;

    ChainX::Material* mMaterial;

};

#endif // EDITMATERIALWIDGET_H
