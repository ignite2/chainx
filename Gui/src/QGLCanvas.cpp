#include <glad/glad.h>

#include "QGLCanvas.h"
#include "Logger.h"

#include <QKeyEvent>



QGLCanvas::QGLCanvas(QWidget* parent)
    :
      QOpenGLWidget(parent),
      mMouseIsMoving(false),
      mRenderer(new ChainX::Renderer)
{
    // Set anti-aliasing for qt canvas
    QSurfaceFormat format;
    format.setSamples(8);
    format.setRenderableType(QSurfaceFormat::OpenGL);
    setFormat(format);

    // Set mouse and keyboard focus
    setFocusPolicy(Qt::ClickFocus);
    setFocus();
}

QGLCanvas::~QGLCanvas()
{
    // Make sure to clear scene within an active gl context, specifically
    // the associated vertex array buffers
    makeCurrent();
    mScene.reset();
    doneCurrent();
}

void QGLCanvas::SetCanvasScene(std::shared_ptr<ChainX::Scene> scene)
{
    mScene = std::move(scene);

    ScanScene();
}

void QGLCanvas::SelectEntity(const std::shared_ptr<ChainX::Entity>& entity)
{
    makeCurrent();
    mRenderer->SetHandleToEntity(entity);
    doneCurrent();
    update();
}

void QGLCanvas::ScanScene()
{
    makeCurrent();
    mRenderer->SetRenderItems(mScene->GetAllEntities(), &mCamera);
    doneCurrent();
    update();
}

void QGLCanvas::initializeGL()
{
    gladLoadGL();

    ChainX::Logger::LogInfo("  Vendor:   {}\n", glGetString(GL_VENDOR));
    ChainX::Logger::LogInfo("  Renderer: {}\n", glGetString(GL_RENDERER));
    ChainX::Logger::LogInfo("  Version:  {}\n", glGetString(GL_VERSION));

    mRenderer->Init();

    mCamera.SetFocusPoint(QVector3D(0.f,0.f,0.f));
    mCamera.SetPerspective(45.f, (float)this->width() / (float)this->height(), .1f, 100.f);
}

void QGLCanvas::resizeGL(int w, int h)
{
    mRenderer->SetRenderSize(w, h);
    mCamera.SetAspect((float)w / (float)h);
    QOpenGLWidget::resizeGL(w, h);
}

void QGLCanvas::paintGL()
{
    mRenderer->RenderFrame();
}

void QGLCanvas::keyPressEvent(QKeyEvent* e)
{
    update();
}

void QGLCanvas::keyReleaseEvent(QKeyEvent* e)
{
    update();
}

void QGLCanvas::mouseMoveEvent(QMouseEvent* e)
{
    // Since (0,0) is the top left, flip the y delta
    float delta_x = e->x() - mLastX;
    float delta_y = mLastY - e->y();

    mLastX = e->x();
    mLastY = e->y();
    mMouseIsMoving = true;

    if (e->modifiers() == Qt::ShiftModifier)
    {
        mCamera.MouseRotate(delta_x, delta_y);
    }
    else if (e->modifiers() == Qt::ControlModifier)
    {
        mCamera.MouseVPan(delta_x, delta_y);
    }
    else
        mCamera.MousePan(delta_x, delta_y);

    update();
}

void QGLCanvas::mousePressEvent(QMouseEvent* e)
{
    if (e->button() == Qt::LeftButton)
    {
        mLastX = e->x();
        mLastY = e->y();
    }
}

void QGLCanvas::mouseReleaseEvent(QMouseEvent* e)
{
    if (e->button() == Qt::LeftButton)
    {
        if (!mMouseIsMoving)
        {
            // TODO: Camera ray cast for mouse pick
            auto picked_entity =
                    mCamera.MousePick(mLastX, height() - mLastY - 1.f,
                              mScene->GetAllEntities(), width(), height());
            SelectEntity(picked_entity);
            emit EntitySelected(picked_entity);
        }

        mMouseIsMoving = false;
    }
}

void QGLCanvas::wheelEvent(QWheelEvent* e)
{
    float delta = e->angleDelta().y();
    mCamera.MouseZoom(delta);

    update();
}

