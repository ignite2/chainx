#include "ToolList.h"

#include <QVBoxLayout>


namespace
{
constexpr short closed_arrow_ucode = 0x25BA; // right
constexpr short opened_arrow_ucode = 0x25BC; // down
constexpr unsigned int label_color = 0xd4d8d4; // light gray
}

//////////////////////////////////////////////////////////////////////////////////////////////////

ToolLabel::ToolLabel(const QString& title)
    :
      QLabel(title)
{
    setMargin(2);

    // Label color
    QPalette pal = palette();
    pal.setColor(backgroundRole(), QColor(label_color));
    setAutoFillBackground(true);
    setPalette(pal);
}

void ToolLabel::mousePressEvent(QMouseEvent* /*event*/)
{
    emit clicked();
}

//////////////////////////////////////////////////////////////////////////////////////////////////

ToolItem::ToolItem(const QString& title, QWidget* item)
    :
      mItem(item)
{
    // A new tool item is always set to opened/expanded
    QString label_title = QChar(opened_arrow_ucode) + ' ' + title;
    mLabel = std::make_unique<ToolLabel>(label_title);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(mLabel.get());
    layout->addWidget(mItem.get());
    setLayout(layout);

    connect(mLabel.get(), SIGNAL(clicked()), this, SLOT(OnLabelClicked()));
}

void ToolItem::OnLabelClicked()
{
    bool display = !mItem->isVisible();
    if (display)
    {
        mItem->setVisible(true);
        auto title = mLabel->text();
        title[0] = QChar(opened_arrow_ucode);
        mLabel->setText(title);
    }
    else
    {
        mItem->setVisible(false);
        auto title = mLabel->text();
        title[0] = QChar(closed_arrow_ucode);
        mLabel->setText(title);
    }
    // An immediate repaint prevents flickering
    if (parent())
        parentWidget()->repaint();
}

//////////////////////////////////////////////////////////////////////////////////////////////////

ToolList::ToolList(QWidget* parent)
    :
      QWidget(parent),
      mLayout(new QVBoxLayout)
{
    mLayout->setSpacing(0);
    mLayout->setContentsMargins(0,0,0,0);
    setLayout(mLayout);
}

ToolList::~ToolList()
{
    Clear();
}

void ToolList::AddItem(ToolItem* item)
{
    auto item_count = mLayout->count();
    if (item_count > 1)
    {
        auto stretch_item = mLayout->itemAt(item_count-1);
        mLayout->removeItem(stretch_item);
        if (stretch_item != nullptr)
            delete stretch_item;
    }

    mLayout->addWidget(item);
    mLayout->addStretch();
}

void ToolList::Clear()
{
    while (QLayoutItem* item = mLayout->takeAt(0))
    {
        if (auto* toolitem = item->widget())
        {
            delete toolitem;
        }
        delete item;
    }
}

void ToolList::paintEvent(QPaintEvent* event)
{
    // Ensure layout updates in case of tool item updates
    mLayout->update();
    QWidget::paintEvent(event);
}
