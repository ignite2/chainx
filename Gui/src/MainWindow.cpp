#include "MainWindow.h"
#include "Entity.h"
#include "ModelImporter.h"
#include "RenderModel.h"

#include <QFileDialog>
#include <QLabel>
#include <QMessageBox>

#include "sandbox/sandbox.h"


MainWindow::MainWindow(QWidget* parent)
    :
      QMainWindow(parent),
      mStatusLabel(new QLabel)
{
    ui.setupUi(this);

    // Connections for ui elements and render canvas
    // Menu bar connections
    connect(ui.mpLoadModelAction, SIGNAL(triggered()), SLOT(LoadModelFile()));
    connect(ui.mpExitAction, SIGNAL(triggered()), SLOT(close()));
    connect(ui.mpAboutAction, SIGNAL(triggered()), SLOT(ShowAbout()));

    connect(ui.mpPropertiesWidget, SIGNAL(PropertiesChanged()), ui.mpGLCanvas, SLOT(update()));
    // Scene connections
    connect(ui.mpGLCanvas, SIGNAL(EntitySelected(const std::shared_ptr<ChainX::Entity>&)),
            ui.mpPropertiesWidget, SLOT(SetEntity(const std::shared_ptr<ChainX::Entity>&)));
    connect(ui.mpSceneWidget, SIGNAL(EntitySelected(const std::shared_ptr<ChainX::Entity>&)),
            ui.mpPropertiesWidget, SLOT(SetEntity(const std::shared_ptr<ChainX::Entity>&)));
    connect(ui.mpSceneWidget, SIGNAL(EntitySelected(const std::shared_ptr<ChainX::Entity>&)),
            ui.mpGLCanvas,    SLOT(SelectEntity(const std::shared_ptr<ChainX::Entity>&)));
    connect(ui.mpSceneWidget, SIGNAL(EntitySelected(const std::shared_ptr<ChainX::Entity>&)),
            this,             SLOT(UpdateStatusBar(const std::shared_ptr<ChainX::Entity>&)));
    connect(ui.mpSceneWidget, SIGNAL(SceneChanged()), ui.mpGLCanvas, SLOT(ScanScene()));

    // Layout settings
    ui.horizontalLayout->setContentsMargins(0,0,0,0);
    ui.mpWidgetsSplitter->setStretchFactor(1,2);

    // Status bar
    mStatusLabel->setAlignment(Qt::AlignCenter);
    ui.statusBar->addWidget(mStatusLabel, 1);
}

MainWindow::~MainWindow()
{
}

void MainWindow::OnStartup()
{
    std::shared_ptr<ChainX::Scene> my_scene(new ChainX::Scene);

    const ChainX::ShaderLibrary& shaders = ui.mpGLCanvas->Renderer()->GetShaderLib();
    ChainX::sandbox::Test(*my_scene, shaders);
    ui.mpGLCanvas->SetCanvasScene(my_scene);

    // Select to the first entity with a mesh component on startup
    for (const auto& ent : ui.mpGLCanvas->Scene()->GetAllEntities())
    {
        if (ent->GetRenderModel() && ent->GetRenderModel()->GetMesh())
        {
            ui.mpPropertiesWidget->SetEntity(ent);
            UpdateStatusBar(ent);
            ui.mpGLCanvas->SelectEntity(ent);
            break;
        }
    }

    ui.mpSceneWidget->SetScene(ui.mpGLCanvas->Scene());
}

void MainWindow::UpdateStatusBar(const std::shared_ptr<ChainX::Entity>& entity)
{
    std::string status_str = entity->GetName();

    ChainX::RenderModel* model = entity->GetRenderModel();
    ChainX::Mesh* mesh{};
    if (model && (mesh = model->GetMesh()))
    {
        auto num_triangles = mesh->NumTriangles();
        status_str += " | Triangles: " + std::to_string(num_triangles);
    }
    else
    {
        status_str += " | Triangles: None";
    }

    mStatusLabel->setText(QString::fromStdString(status_str));
}

void MainWindow::LoadModelFile()
{
    auto default_dir = QDir::homePath() + "\\Downloads";
    QString stlFile =
            QFileDialog::getOpenFileName(this, "Choose STL File to Load", default_dir);

    if (!stlFile.isEmpty())
    {
        ChainX::ModelImporter modelImporter(ui.mpGLCanvas->Scene(),
                                            ui.mpGLCanvas->Renderer());
        std::shared_ptr<ChainX::Entity> stl_ent = modelImporter.Load(stlFile.toStdString());

        ui.mpGLCanvas->makeCurrent();
        ChainX::FinalizeEntity(stl_ent);
        ui.mpGLCanvas->doneCurrent();

        // Scan scene for display canvas after updates
        ui.mpGLCanvas->ScanScene();
        ui.mpSceneWidget->SetScene(ui.mpGLCanvas->Scene());
    }
}

void MainWindow::ShowAbout()
{
    QMessageBox::about(this, "About Chain X",
                       "Chain X is a 3D Graphics Engine for Modeling + Simulation\n"
                       "\n"
                       "Version: Unreleased / In Development");
}








