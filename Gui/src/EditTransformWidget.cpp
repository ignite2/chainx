#include "EditTransformWidget.h"
#include "ui_EditTransformWidget.h"

#include "Transform.h"


EditTransformWidget::EditTransformWidget(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::EditTransformWidget)
{
    ui->setupUi(this);

    connect(ui->mpPosXSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnPosUpdate()));
    connect(ui->mpPosYSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnPosUpdate()));
    connect(ui->mpPosZSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnPosUpdate()));

    connect(ui->mpRotXSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnRotUpdate()));
    connect(ui->mpRotYSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnRotUpdate()));
    connect(ui->mpRotZSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnRotUpdate()));

    connect(ui->mpScaleXSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnScaleUpdate()));
    connect(ui->mpScaleYSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnScaleUpdate()));
    connect(ui->mpScaleZSpinBox, SIGNAL(textChanged(const QString&)), this, SLOT(OnScaleUpdate()));

    QString deg_symbol = QString(" ") + QChar(0x00B0); // deg symbol unicode
    ui->mpRotXSpinBox->setSuffix(deg_symbol);
    ui->mpRotYSpinBox->setSuffix(deg_symbol);
    ui->mpRotZSpinBox->setSuffix(deg_symbol);
}

EditTransformWidget::~EditTransformWidget()
{
    delete ui;
}

void EditTransformWidget::SetTransform(ChainX::Transform* transform)
{
    mTransform = transform;

    const QSignalBlocker blocker(this);

    auto pos = mTransform->Position();
    ui->mpPosXSpinBox->setValue(pos.x());
    ui->mpPosYSpinBox->setValue(pos.y());
    ui->mpPosZSpinBox->setValue(pos.z());
    auto euler = mTransform->Rotation().toEulerAngles();
    ui->mpRotXSpinBox->setValue(euler.x());
    ui->mpRotYSpinBox->setValue(euler.y());
    ui->mpRotZSpinBox->setValue(euler.z());
    auto scale = mTransform->Scale();
    ui->mpScaleXSpinBox->setValue(scale.x());
    ui->mpScaleYSpinBox->setValue(scale.y());
    ui->mpScaleZSpinBox->setValue(scale.z());
}

void EditTransformWidget::OnPosUpdate()
{
    QVector3D pos {
        static_cast<float>(ui->mpPosXSpinBox->value()),
        static_cast<float>(ui->mpPosYSpinBox->value()),
        static_cast<float>(ui->mpPosZSpinBox->value()),
    };
    mTransform->SetPosition(pos);

    emit TransformChanged();
}

void EditTransformWidget::OnRotUpdate()
{
    float x = ui->mpRotXSpinBox->value();
    float y = ui->mpRotYSpinBox->value();
    float z = ui->mpRotZSpinBox->value();
    mTransform->SetRotation(x, y, z);

    emit TransformChanged();
}

void EditTransformWidget::OnScaleUpdate()
{
    QVector3D scale {
        static_cast<float>(ui->mpScaleXSpinBox->value()),
        static_cast<float>(ui->mpScaleYSpinBox->value()),
        static_cast<float>(ui->mpScaleZSpinBox->value()),
    };
    mTransform->SetScale(scale);

    emit TransformChanged();
}


