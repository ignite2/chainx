#include "PropertiesWidget.h"
#include "ToolList.h"
#include "EditMaterialWidget.h"
#include "EditTransformWidget.h"
#include "Entity.h"
#include "RenderModel.h"

#include <QVBoxLayout>

PropertiesWidget::PropertiesWidget(QWidget* parent)
    : QWidget(parent),
      mScrollList(new QScrollArea(this))
{
    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 10, 0);
    layout->addWidget(mScrollList.get());

    mScrollList->setWidgetResizable(true);
    mScrollList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mScrollList->setFrameStyle(QFrame::NoFrame);
    mScrollList->setWidget(new ToolList);

    setLayout(layout);
}

void PropertiesWidget::SetEntity(const std::shared_ptr<ChainX::Entity>& entity)
{
    mScrollList->hide();
    auto properties_list = static_cast<ToolList*>(mScrollList->widget());
    properties_list->Clear();

    if (entity == nullptr)
        return;

    if (auto transform = entity->GetTransform())
    {
        EditTransformWidget* transform_widget = new EditTransformWidget;
        transform_widget->SetTransform(transform);
        ToolItem* transform_item = new ToolItem("Transform", transform_widget);
        properties_list->AddItem(transform_item);

        connect(transform_widget, SIGNAL(TransformChanged()), SIGNAL(PropertiesChanged()));
    }
    ChainX::RenderModel* model = entity->GetRenderModel();
    ChainX::Material* material{};
    if (model && (material = model->GetMaterial()))
    {
        EditMaterialWidget* edit_mat = new EditMaterialWidget;
        edit_mat->SetMaterial(material);
        ToolItem* mat_item = new ToolItem("Material", edit_mat);
        properties_list->AddItem(mat_item);

        connect(edit_mat, SIGNAL(MaterialChanged()), SIGNAL(PropertiesChanged()));
    }
    mScrollList->show();
}
