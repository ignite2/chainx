#include "EditMaterialWidget.h"
#include "ui_EditMaterialWidget.h"

#include <cmath>
#include <QColorDialog>


namespace
{
inline auto RgbVectorToQColor(const QVector3D& rgb)
{
    return QColor(std::round(rgb[0]*255), std::round(rgb[1]*255), std::round(rgb[2]*255));
}

void SetButtonColor(QPushButton* btn, const QColor& rgbColor)
{
    QPalette pal = btn->palette();
    pal.setColor(QPalette::Button, rgbColor);

    btn->setFlat(true);
    btn->setAutoFillBackground(true);
    btn->setPalette(pal);
    btn->update();
}
}


EditMaterialWidget::EditMaterialWidget(QWidget *parent)
    :
      QWidget(parent),
      ui(new Ui::EditMaterialWidget)
{
    ui->setupUi(this);

    connect(ui->mpDiffColorBtn, SIGNAL(clicked()), this, SLOT(OnChangeBaseColor()));
    connect(ui->mpSpecColorBtn, SIGNAL(clicked()), this, SLOT(OnChangeSpecColor()));
    connect(ui->mpSpecPowerSpinBox, SIGNAL(valueChanged(double)), this, SLOT(OnSpecPowerChanged(double)));
    connect(ui->mpSpecStrengthSpinBox, SIGNAL(valueChanged(double)), this, SLOT(OnSpecStrengthChanged(double)));

    // Init widgets
    SetButtonColor(ui->mpDiffColorBtn, Qt::white);
    SetButtonColor(ui->mpSpecColorBtn, Qt::white);
}

EditMaterialWidget::~EditMaterialWidget()
{
    delete ui;
}

void EditMaterialWidget::SetMaterial(ChainX::Material* material)
{
    mMaterial = material;

    SetButtonColor(ui->mpDiffColorBtn, RgbVectorToQColor(mMaterial->BaseColor()));
    SetButtonColor(ui->mpSpecColorBtn, RgbVectorToQColor(mMaterial->SpecularColor()));
    ui->mpSpecPowerSpinBox->setValue(mMaterial->SpecularPower());
    ui->mpSpecStrengthSpinBox->setValue(mMaterial->SpecularStrength());
}

void EditMaterialWidget::OnChangeBaseColor()
{
    auto init_color = RgbVectorToQColor(mMaterial->BaseColor());

    QColorDialog* color_dialog = new QColorDialog(init_color, this);

    connect(color_dialog, SIGNAL(currentColorChanged(const QColor&)),
            this, SLOT(OnBaseColorChanged(const QColor&)));

    color_dialog->adjustSize();
    color_dialog->setAttribute(Qt::WA_DeleteOnClose);
    color_dialog->open();
}

void EditMaterialWidget::OnChangeSpecColor()
{
    auto init_color = RgbVectorToQColor(mMaterial->SpecularColor());

    QColorDialog* color_dialog = new QColorDialog(init_color, this);

    connect(color_dialog, SIGNAL(currentColorChanged(const QColor&)),
            this, SLOT(OnSpecColorChanged(const QColor&)));

    color_dialog->adjustSize();
    color_dialog->setAttribute(Qt::WA_DeleteOnClose);
    color_dialog->open();
}

void EditMaterialWidget::OnBaseColorChanged(const QColor& color)
{
    mMaterial->SetBaseColor(QVector3D(color.redF(), color.greenF(), color.blueF()));
    SetButtonColor(ui->mpDiffColorBtn, color);

    emit MaterialChanged();
}

void EditMaterialWidget::OnSpecColorChanged(const QColor& color)
{
    mMaterial->SetSpecularColor(QVector3D(color.redF(), color.greenF(), color.blueF()));
    SetButtonColor(ui->mpSpecColorBtn, color);

    emit MaterialChanged();
}

void EditMaterialWidget::OnSpecPowerChanged(double value)
{
    mMaterial->SetSpecularPower(value);

    emit MaterialChanged();
}

void EditMaterialWidget::OnSpecStrengthChanged(double value)
{
    mMaterial->SetSpecularStrength(value);

    emit MaterialChanged();
}


