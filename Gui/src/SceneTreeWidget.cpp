#include "SceneTreeWidget.h"
#include "Scene.h"
#include "Entity.h"
#include "Transform.h"

#include <QKeyEvent>
#include <QTreeWidget>
#include <QVBoxLayout>
#include <QMenu>

namespace
{
void AddEntityToTreeItem(QTreeWidgetItem* parentItem, const std::shared_ptr<ChainX::Entity>& entity)
{
    auto* item = new QTreeWidgetItem;
    item->setData(0, Qt::DisplayRole, QVariant{ QString::fromStdString(entity->GetName()) });
    item->setData(0, Qt::UserRole,    QVariant{ entity->GetId() } );

    parentItem->addChild(item);

    for (const auto& child_entt : entity->GetChildren())
    {
        AddEntityToTreeItem(item, child_entt);
    }
}
}

SceneTreeWidget::SceneTreeWidget(QWidget* parent)
    : QWidget(parent),
      mSceneTree(new QTreeWidget(parent))
{
    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 5, 10, 0);
    layout->addWidget(mSceneTree.get());
    setLayout(layout);

    mSceneTree->setHeaderHidden(true);
    mSceneTree->setColumnCount(1);
    mSceneTree->setIndentation(15);
    mSceneTree->setAlternatingRowColors(true);

    connect(mSceneTree.get(), SIGNAL(itemSelectionChanged()),
            this, SLOT(OnItemSelected()));

    // Context menu
    connect(mSceneTree.get(), SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(OnCtxMenu(const QPoint&)));
    mSceneTree->setContextMenuPolicy(Qt::CustomContextMenu);
}

void SceneTreeWidget::SetScene(ChainX::Scene* scene)
{
    mSceneTree->clear();

    mScene = scene;

    auto root_ent = mScene->GetRootEntity();
    auto* root_item = new QTreeWidgetItem;
    root_item->setData(0, Qt::DisplayRole, QVariant{ "Scene" });
    root_item->setData(0, Qt::UserRole,    QVariant{ root_ent->GetId() });
    mSceneTree->addTopLevelItem(root_item);

    for (const auto& entity : root_ent->GetChildren())
    {
        AddEntityToTreeItem(root_item, entity);
    }

    mSceneTree->expandItem(root_item);
}

void SceneTreeWidget::OnItemSelected()
{
    auto* item = mSceneTree->currentItem();
    auto entt_id = item->data(0, Qt::UserRole).value<std::uint32_t>();
    auto entity = mScene->GetEntity(entt_id);

    emit EntitySelected(entity);
}

void SceneTreeWidget::OnCtxMenu(const QPoint& pos)
{
    QMenu menu(this);

    auto item = mSceneTree->itemAt(pos);
    if (item)
    {
        QAction* delete_action = new QAction("Delete Entity", this);
        connect(delete_action, SIGNAL(triggered()), this, SLOT(OnDeleteAction()));
        menu.addAction(delete_action);
    }

    menu.exec(mSceneTree->mapToGlobal(pos));
}

void SceneTreeWidget::OnDeleteAction()
{
    DeleteSceneItem(mSceneTree->currentItem());
}

void SceneTreeWidget::keyPressEvent(QKeyEvent* e)
{
    if (e->key() == Qt::Key_Delete)
    {
        auto item = mSceneTree->currentItem();
        if (item)
            DeleteSceneItem(item);
    }
}

void SceneTreeWidget::DeleteSceneItem(QTreeWidgetItem* item)
{
    auto ent_id = item->data(0, Qt::UserRole).value<std::uint32_t>();
    auto entity = mScene->GetEntity(ent_id);
    auto parent_entity = entity->Parent();
    mScene->DeleteEntity(entity);
    // Reset parent transform hierarchy of entity to delete
    parent_entity->GetTransform()->SetChildren(parent_entity->GetChildren());

    // Update scene tree widget after deletion
    if (auto* parent = item->parent())
    {
        parent->removeChild(item);
        delete item;
    }
    else
    {
        // Top level "Scene" item. Clear scene children.
        // Possibly handle multiple scenes in the future, but not now so keep scene.
        qDeleteAll(item->takeChildren());
        // Reset the root scene item to the new root id
        auto root_ent = mScene->GetRootEntity();
        item->setData(0, Qt::UserRole, QVariant{ root_ent->GetId() });
    }

    emit SceneChanged();
}

