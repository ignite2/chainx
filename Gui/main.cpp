#include "MainWindow.h"
#include "Logger.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    ChainX::Logger::Init();

    QApplication a(argc, argv);

    Q_INIT_RESOURCE(CXResources);

    MainWindow w;
    w.show();
    w.OnStartup();

    return a.exec();
}
