#ifndef COMMON_BUFFER
#define COMMON_BUFFER

//layout (std140) uniform CommonUBO
//{
//};

layout (std140, binding = 0) uniform MatricesUBO
{
    mat4 view;
    mat4 projection;
};

layout (std140, binding = 1) uniform SceneUBO
{
    vec4 viewPos;
    DirectionalLight dirLights[2];   // MAX_DIR_LIGHTS   2
    PointLight       pointLights[8]; // MAX_POINT_LIGHTS 8
    int activeDirLights;
    int activePointLights;
};

#endif
