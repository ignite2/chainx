#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

//#include common_buffer.glsl
layout (std140, binding = 0) uniform MatricesUBO
{
    mat4 view;
    mat4 projection;
};

out vec3 FinalColor;

uniform mat4 model;

void main(void)
{
    FinalColor = color;

    gl_Position = projection * view * model * vec4(position, 1.0);
}
