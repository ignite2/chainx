#version 450 core

out vec4 FragColor;

in vec3 FinalColor;

void main(void)
{
   FragColor = vec4(FinalColor, 1.);
}
