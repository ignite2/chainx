#version 450 core

layout (location = 0) in vec3 position;
layout (location = 2) in vec2 uvCoord;

out vec2 TexUV0;

//#include common_buffer.glsl
layout (std140, binding = 0) uniform MatricesUBO
{
    mat4 view;
    mat4 projection;
};

uniform mat4 model;

void main()
{
    TexUV0 = uvCoord;

    gl_Position = projection * view * vec4(position, 1.0);
}
