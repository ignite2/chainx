#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uvCoord;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexUV0;

//#include common_buffer.glsl
layout (std140, binding = 0) uniform MatricesUBO
{
    mat4 view;
    mat4 projection;
};

uniform mat4 model;

void main()
{
    FragPos = vec3(model * vec4(position, 1.0));
    Normal = mat3(transpose(inverse(model))) * normal;
    TexUV0 = uvCoord;

    gl_Position = projection * view * vec4(FragPos, 1.0);
}
