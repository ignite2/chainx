#version 450 core

out vec4 FragColor;

in vec2 TexUV0;

uniform sampler2D uTexture;

void main(void)
{
    FragColor = texture(uTexture, TexUV0);
}
