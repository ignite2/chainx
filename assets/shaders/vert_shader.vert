#version 450 core

layout (location = 0) in vec3 position;

//#include common_buffer.glsl
layout (std140, binding = 0) uniform MatricesUBO
{
    mat4 view;
    mat4 projection;
};

uniform mat4 model;

void main(void)
{
    gl_Position = projection * view * model * vec4(position, 1.0);
}
