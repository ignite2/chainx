#version 450 core

// Material for Fragment shader
struct Material
{
    vec3 diffuse;
    vec3 specular;
    float specularPower;
    float specularStrength;
};
////////////////////////////////////////////////////////////////////////////////
// Common structs and Scene UBO
struct DirectionalLight
{
    vec3 direction;
    // color
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight
{
    vec4 position;
    vec4 color;
    // attenuation
    float constant;
    float linear;
    float quadratic;
};

layout (std140, binding = 1) uniform SceneUBO
{
    vec4 viewPos;
    DirectionalLight dirLights[2];   // MAX_DIR_LIGHTS   2
    PointLight       pointLights[8]; // MAX_POINT_LIGHTS 8
    int activeDirLights;
    int activePointLights;
};
////////////////////////////////////////////////////////////////////////////////

// Output
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec3 FragNormal;

// Inputs from vertex shader
in vec3 FragPos;
in vec3 Normal;
in vec2 TexUV0;

// Uniforms
uniform Material material;
uniform sampler2D uTexture;

// Function Declarations
vec3 CalcDirLight(DirectionalLight light, vec3 diffColor, vec3 specColor,
                  vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light, vec3 diffColor, vec3 specColor,
                    vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
    vec3 fragNormal = normalize(Normal);
    FragNormal = fragNormal;

    vec3 viewDir = normalize(viewPos.xyz - FragPos);

    vec3 color = material.diffuse;
    vec3 specularColor = material.specular * material.specularStrength;
#if TEXTURE_MAP
    color *= vec3(texture(uTexture, TexUV0));
#endif

    vec3 final_color = vec3(0,0,0);
    // directional lights
    for (int i = 0; i < activeDirLights; ++i)
    {
        final_color += CalcDirLight(dirLights[i], color, specularColor, fragNormal, viewDir);
    }
    // point lights
    for (int i = 0; i < activePointLights; ++i)
    {
        final_color += CalcPointLight(pointLights[i], color, specularColor,
                                 fragNormal, FragPos, viewDir);
    }

    // Add small amount of ambient color
    final_color += color * 0.1;

    FragColor = vec4(final_color, 1.0);
}

////////////////////////////////////////////////////////////////////////////////

// Calculate color from directional light
vec3 CalcDirLight(DirectionalLight light, vec3 diffColor, vec3 specColor,
                  vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(-light.direction.xyz);

    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);

    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.specularPower);

    // combine results
    vec3 ambient  = light.ambient.rgb  * diffColor;
    vec3 diffuse  = light.diffuse.rgb  * diff * diffColor;
    vec3 specular = light.specular.rgb * spec * specColor;

    return (ambient + diffuse + specular);
}

// Calculate color from point light
vec3 CalcPointLight(PointLight light, vec3 diffColor, vec3 specColor,
                    vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position.xyz - fragPos);

    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.specularPower);

    // attenuation
    float distance = length(light.position.xyz - fragPos);
    float attenuation = 1.0 /
            (light.constant + light.linear * distance + light.quadratic * (distance * distance));

    // combine results
    vec3 irradiance = light.color.rgb
            * ((diff * diffColor) + (spec * specColor)) * attenuation;

    return irradiance;
}
