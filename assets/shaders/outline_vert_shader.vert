#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

//#include common_buffer.glsl
layout (std140, binding = 0) uniform MatricesUBO
{
    mat4 view;
    mat4 projection;
};

uniform mat4 model;
uniform vec3 scale;

void main(void)
{
    vec3 pos = vec3(model * vec4(position, 1.0));
    // Use scale input directly for correctly-scaled outline.
    // Ensure orthogonal normal with normal matrix from model.
    vec3 norm = mat3(transpose(inverse(model))) * (normal * scale);

    gl_Position = projection * view * vec4(pos + norm * 0.05, 1.0);
}
