#-------------------------------------------------
#
#-------------------------------------------------


QT += core opengl
QT += widgets

win32 {
QMAKE_CXXFLAGS += /std:c++17
LIBS += opengl32.lib -L$$PWD/build -lassimp-vc142-mt
}

linux {
QMAKE_CXXFLAGS += -std=c++17 -fno-sized-deallocation
LIBS += -ldl -L/opt/tools/lib -lassimp
}

TARGET = ChainXModeler
TEMPLATE = app

INCLUDEPATH +=       \
    Engine/Core/include \
    Engine/IO/include   \
    Engine/Math/include \
    Engine/Renderer/Gizmos/include \
    Engine/Renderer/include \
    Engine/Scene/include    \
    Engine/Scene/Components/include \
    Gui/include \
    libGLExt/include \
    Thirdparty

HEADERS += \
    Engine/Core/include/Context.h \
    Engine/Core/include/Logger.h \
    Engine/Core/include/Module.h \
    Engine/IO/include/ModelImporter.h \
    Engine/Math/include/BoundingBox.h \
    Engine/Math/include/HitResult.h \
    Engine/Math/include/Ray.h \
    Engine/Math/include/calc_mesh.h \
    Engine/Math/include/constants.h   \
    Engine/Math/include/float_equal.h \
    Engine/Renderer/Gizmos/include/Grid.h \
    Engine/Renderer/Gizmos/include/PositionHandle.h \
    Engine/Renderer/Gizmos/include/SelectedEntityHandle.h \
    Engine/Renderer/Gizmos/include/TransformHandle.h \
    Engine/Renderer/Gizmos/include/TransformHandleAxis.h \
    Engine/Renderer/include/Camera.h  \
    Engine/Renderer/include/GLState.h \
    Engine/Renderer/include/Material.h  \
    Engine/Renderer/include/Mesh.h     \
    Engine/Renderer/include/Renderer.h  \
    Engine/Renderer/include/RenderCommand.h \
    Engine/Renderer/include/ShaderLibrary.h \
    Engine/Renderer/include/Texture.h \
    Engine/Renderer/include/mesh_shapes.h  \
    Engine/Renderer/include/shader_utils.h \
    Engine/Scene/Components/include/Component.h \
    Engine/Scene/Components/include/DirectionalLight.h \
    Engine/Scene/Components/include/PointLight.h \
    Engine/Scene/Components/include/RenderModel.h \
    Engine/Scene/Components/include/Transform.h \
    Engine/Scene/include/Entity.h \
    Engine/Scene/include/Scene.h \
    Gui/include/EditTransformWidget.h \
    Gui/include/MainWindow.h           \
    Gui/include/PropertiesWidget.h \
    Gui/include/QGLCanvas.h            \
    Gui/include/EditMaterialWidget.h   \
    Gui/include/SceneTreeWidget.h \
    Gui/include/ToolList.h \
    libGLExt/include/glad/glad.h       \
    libGLExt/include/KHR/khrplatform.h \
    Thirdparty/stb_image/stb_image.h \
    sandbox/sandbox.h

SOURCES += \
    Engine/Core/src/Context.cpp \
    Engine/Core/src/Logger.cpp \
    Engine/Core/src/Module.cpp \
    Engine/IO/src/ModelImporter.cpp \
    Engine/Math/src/BoundingBox.cpp \
    Engine/Math/src/Ray.cpp \
    Engine/Math/src/calc_mesh.cpp \
    Engine/Renderer/Gizmos/src/Grid.cpp \
    Engine/Renderer/Gizmos/src/PositionHandle.cpp \
    Engine/Renderer/Gizmos/src/SelectedEntityHandle.cpp \
    Engine/Renderer/Gizmos/src/TransformHandle.cpp \
    Engine/Renderer/Gizmos/src/TransformHandleAxis.cpp \
    Engine/Renderer/src/Camera.cpp      \
    Engine/Renderer/src/GLState.cpp \
    Engine/Renderer/src/Material.cpp    \
    Engine/Renderer/src/Mesh.cpp        \
    Engine/Renderer/src/Renderer.cpp    \
    Engine/Renderer/src/ShaderLibrary.cpp \
    Engine/Renderer/src/Texture.cpp \
    Engine/Renderer/src/mesh_shapes.cpp \
    Engine/Renderer/src/shader_utils.cpp \
    Engine/Scene/Components/src/RenderModel.cpp \
    Engine/Scene/Components/src/Transform.cpp \
    Engine/Scene/src/Entity.cpp  \
    Engine/Scene/src/Scene.cpp   \
    Gui/main.cpp \
    Gui/src/EditTransformWidget.cpp \
    Gui/src/MainWindow.cpp       \
    Gui/src/PropertiesWidget.cpp \
    Gui/src/QGLCanvas.cpp        \
    Gui/src/EditMaterialWidget.cpp \
    Gui/src/SceneTreeWidget.cpp \
    Gui/src/ToolList.cpp \
    libGLExt/src/glad.c  \
    Thirdparty/stb_image/stb_image.cpp \
    sandbox/sandbox.cpp

FORMS += Gui/ui/MainWindow.ui \
    Gui/ui/EditMaterialWidget.ui \
    Gui/ui/EditTransformWidget.ui

RESOURCES += \
    assets/CXResources.qrc

