#ifndef MODELIMPORTER_H
#define MODELIMPORTER_H


#include <memory>
#include <string>

struct aiNode;
struct aiScene;

namespace ChainX
{
class Entity;
class Scene;
class Renderer;
class Material;

/**
 * @brief Utility function to finalize meshes for an entity and
 * it's children. This is necessary after a model is imported.
 * Must run within a valid OpenGL context
 */
void FinalizeEntity(const std::shared_ptr<ChainX::Entity>& entity);

class ModelImporter
{
public:
    ModelImporter(Scene* scene, Renderer* renderer);

    /**
     * @brief Loads model from the provided file into the current scene
     * @return Entity ptr to the head/principal entity of the model or
     * nullptr if load was not successful
     */
    std::shared_ptr<Entity> Load(const std::string& filename);

private:
    void ProcessNode(const aiNode* node, const aiScene* scene,
                     Entity* nodeEntity, Entity* parent);

    Scene* mScene;
    Renderer* mRenderer;
};
}

#endif // MODELIMPORTER_H
