#include "ModelImporter.h"
#include "Entity.h"
#include "Scene.h"
#include "Renderer.h"
#include "Mesh.h"
#include "Material.h"
#include "RenderModel.h"
#include "Transform.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <filesystem>

namespace ChainX
{
void FinalizeEntity(const std::shared_ptr<Entity>& entity)
{
    RenderModel* model = entity->GetRenderModel();
    if (model)
    {
        Mesh* mesh = model->GetMesh();
        mesh->Finalize(model->GetMaterial()->Shader());
    }
    for (const auto& child : entity->GetChildren())
    {
        FinalizeEntity(child);
    }
}


namespace
{
std::shared_ptr<Material> GetMaterialInstance(const ShaderLibrary& shaderLib)
{
    auto shader = shaderLib.GetShader(ShaderType::GPU_DEFAULT);
    std::shared_ptr<Material> mat(new Material(shader));
    mat->SetBaseColor({.8f, .8f, .8f});

    return mat;
}
}


ModelImporter::ModelImporter(Scene* scene, Renderer* renderer)
    :
      mScene(scene),
      mRenderer(renderer)
{
}

std::shared_ptr<Entity> ModelImporter::Load(const std::string& filename)
{
    if (mScene == nullptr)
    {
        return nullptr;
    }

    // aiPostProcessSteps flags
    const auto import_flags =
            aiProcess_Triangulate |
            aiProcess_SortByPType;
        // Possible flags for future features/enhancements. Not used now
        //  aiProcess_GenSmoothNormals |
        //  aiProcess_CalcTangentSpace |
        //  aiProcess_JoinIdenticalVertices |
        //  aiProcess_FlipUVs |
        //  aiProcess_GenUVCoords |
    Assimp::Importer importer;
    importer.SetPropertyInteger(
                AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_LINE | aiPrimitiveType_POINT);

    if (const aiScene* ai_scene = importer.ReadFile(filename, import_flags))
    {
        // Create root entity to Assimp's root
        std::shared_ptr<Entity> model_root = mScene->CreateEntity();
        model_root->SetName(std::filesystem::path(filename).stem().string());

        ProcessNode(ai_scene->mRootNode, ai_scene, model_root.get(), nullptr);

        return model_root;
    }
    else
    {
        // TODO: add logging
        return nullptr;
    }
}

std::shared_ptr<Mesh> LoadMesh(const aiMesh* aMesh)
{
    std::uint32_t num_indices = aMesh->mNumFaces * 3;

    std::vector<QVector3D> vertices(aMesh->mNumVertices);
    std::vector<QVector3D> normals(aMesh->mNumVertices);
    std::vector<std::uint32_t> indices(num_indices);

    for (auto i = 0u; i < aMesh->mNumVertices; ++i)
    {
        auto vert3 = aMesh->mVertices[i];
        auto norm3 = aMesh->mNormals[i];
        vertices[i] = QVector3D(vert3.x, vert3.y, vert3.z);
        normals[i]  = QVector3D(norm3.x, norm3.y, norm3.z);
    }

    for (auto i = 0u; i < aMesh->mNumFaces; ++i)
    {
        auto index = i * 3;
        indices[index]   = aMesh->mFaces[i].mIndices[0];
        indices[index+1] = aMesh->mFaces[i].mIndices[1];
        indices[index+2] = aMesh->mFaces[i].mIndices[2];
    }

    return std::shared_ptr<Mesh>( new Mesh(std::move(vertices), std::move(normals), std::move(indices)) );
}

void ModelImporter::ProcessNode(const aiNode* node, const aiScene* scene,
                                Entity* nodeEntity, Entity* parentEntity)
{
    // Process meshes
    for (auto i = 0u; i < node->mNumMeshes; ++i)
    {
        Entity* entity = nodeEntity;
        aiMesh* assimp_mesh = scene->mMeshes[node->mMeshes[i]];
        auto mesh = LoadMesh(assimp_mesh);

        if (node->mNumMeshes > 1)
        {
            entity = mScene->CreateEntity(nodeEntity).get();
            entity->SetName(nodeEntity->GetName() + "_" + std::to_string(i+1));
        }

        // TODO: Process materials/textures from inputs. Use default plain material for now
        std::shared_ptr<RenderModel> render_model(
                new RenderModel(std::move(mesh), GetMaterialInstance(mRenderer->GetShaderLib()), entity) );
        entity->SetRenderModel(render_model);
    }

    // Process children of node entity
    for (auto i = 0u; i < node->mNumChildren; ++i)
    {
        std::shared_ptr<Entity> child_ent = mScene->CreateEntity(nodeEntity);

        std::string child_name(node->mChildren[i]->mName.C_Str());
        // If no child name, or if empty, append "child<num>" to parent name
        if ( child_name.empty() )
        {
            child_name = nodeEntity->GetName() + "_c" + std::to_string(i+1);
        }
        child_ent->SetName(child_name);
        ProcessNode(node->mChildren[i], scene, child_ent.get(), nodeEntity);
    }
    // Set transform hierarchy of children for this node
    nodeEntity->GetTransform()->SetChildren(nodeEntity->GetChildren());
}
}




