#include "Transform.h"
#include "Entity.h"

namespace ChainX
{
Transform::Transform()
{
}

void Transform::SetPosition(const QVector3D& pos)
{
    mPosition = pos;
    UpdateMatrix();
}

void Transform::SetScale(const QVector3D& scale)
{
    mScale = scale;
    UpdateMatrix();
}

void Transform::SetRotation(const QQuaternion& rotation)
{
    mRotation = rotation;
    UpdateMatrix();
}

void Transform::SetRotation(float xRot, float yRot, float zRot)
{
    mRotation = QQuaternion::fromEulerAngles(xRot, yRot, zRot);
    UpdateMatrix();
}

void Transform::Translate(const QVector3D& delta)
{
    if (mParent)
    {
        mPosition = WorldPosition() + delta;
    }
    else
    {
        mPosition += delta;
    }
    UpdateMatrix();
}

void Transform::Rotate(float deltaX, float deltaY, float deltaZ)
{
    Rotate( QQuaternion::fromEulerAngles(deltaX, deltaY, deltaZ) );
}

void Transform::Rotate(const QQuaternion& delta)
{
    if (mParent)
    {
        QQuaternion model_rot = WorldRotation();

        mRotation = mRotation * model_rot.inverted() * delta * model_rot;
    }
    else
    {
        mRotation = (mRotation * delta).normalized();
    }
}

QVector3D Transform::WorldPosition() const
{
    return { mMatrix(0, 3), mMatrix(1,3), mMatrix(2,3) };
}

QVector3D Transform::WorldScale() const
{
    QVector3D x(mMatrix(0,0), mMatrix(1,0), mMatrix(2,0));
    QVector3D y(mMatrix(0,1), mMatrix(1,1), mMatrix(2,1));
    QVector3D z(mMatrix(0,2), mMatrix(1,2), mMatrix(2,2));

    return { x.length(), y.length(), z.length() };
}

QQuaternion Transform::WorldRotation() const
{
    QVector3D scale = WorldScale();

    if (scale.x() == 0.f || scale.y() == 0.f || scale.z() == 0.f)
        return QQuaternion(1,0,0,0);

    QMatrix3x3 rot_matrix;
    rot_matrix(0,0) = mMatrix(0,0) / scale.x();
    rot_matrix(1,0) = mMatrix(1,0) / scale.x();
    rot_matrix(2,0) = mMatrix(2,0) / scale.x();
    rot_matrix(0,1) = mMatrix(0,1) / scale.y();
    rot_matrix(1,1) = mMatrix(1,1) / scale.y();
    rot_matrix(2,1) = mMatrix(2,1) / scale.y();
    rot_matrix(0,2) = mMatrix(0,2) / scale.z();
    rot_matrix(1,2) = mMatrix(1,2) / scale.z();
    rot_matrix(2,2) = mMatrix(2,2) / scale.z();

    return QQuaternion::fromRotationMatrix(rot_matrix);
}

const QMatrix4x4& Transform::GetMatrix()
{
    return mMatrix;
}

void Transform::UpdateMatrix()
{
    mMatrix.setToIdentity();
    mMatrix.translate(mPosition);
    mMatrix.rotate(mRotation);
    mMatrix.scale(mScale);

    if (mParent)
    {
        mMatrix = mParent->GetMatrix() * mMatrix;
    }

    for (auto child : mChildren)
    {
        child->UpdateMatrix();
    }
}

void Transform::ClearChildren()
{
    for (auto child : mChildren)
    {
        child->SetParent(nullptr);
    }
    mChildren.clear();
}

void Transform::SetChildren(const std::vector<std::shared_ptr<Entity>>& childEntites)
{
    ClearChildren();
    for (const auto& child : childEntites)
    {
        auto child_transform = child->GetTransform();
        AddChild(child_transform);

        if (child->HasChildren())
        {
            child->GetTransform()->SetChildren(child->GetChildren());
        }
    }
}

void Transform::SetParent(Transform* parent)
{
    mParent = parent;

    if (mParent != nullptr)
        mParent->mChildren.push_back(this);
}

void Transform::AddChild(Transform* child)
{
    if (mParent == child || child == nullptr)
        return;

    mChildren.push_back(child);
    child->mParent = this;
}

}

