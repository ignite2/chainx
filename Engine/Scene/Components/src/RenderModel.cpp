#include "RenderModel.h"
#include "Mesh.h"
#include "Transform.h"

namespace ChainX
{
RenderModel::RenderModel(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material,
                         Entity* entity)
    :
      Component(entity),
      mMesh(std::move(mesh)),
      mMaterial(std::move(material)),
      mBoundingBox(mMesh->Vertices()),
      mAABB(mBoundingBox)
{
}

const math::BoundingBox& RenderModel::GetBoundingBox()
{
    // Check if model's bounding box needs udpating from entity's transform
    if (mPrevTransform != mTransform->GetMatrix())
    {
        mAABB = mBoundingBox.GetAABB(mTransform->GetMatrix());
        mPrevTransform = mTransform->GetMatrix();
    }
    return mAABB;
}
}


