#pragma once

#include "Entity.h"

namespace ChainX
{
class Transform;

class Component
{
public:
    explicit Component(Entity* entity)
        : mEntity(entity) { mTransform = mEntity->GetTransform(); };
    virtual ~Component() = default;

protected:
    Entity*    mEntity;
    Transform* mTransform;
};
}


