#pragma once

#include <memory>
#include <QVector4D>


namespace ChainX
{
/**
 * @brief Struct for a point light source component
 */
struct PointLight
{
    QVector4D position;
    QVector4D color;
    float constant;
    float linear;
    float quadratic;
    //float radius; // Replace old attenuation model?
    float pad; // Need to pad struct to multiple of 16
};

// PointLight struct layout info for GLSL buffer objects
inline constexpr unsigned int sizeof_PointLight = sizeof(PointLight);
inline constexpr unsigned int offsets_PointLight[] =
{
    0, 16, 32, 36, 40, 44
};

/**
 * @brief Small function to create a Point Light component
 */
inline std::shared_ptr<PointLight> CreatePointLight(QVector4D color, QVector4D position)
{
    return std::shared_ptr<PointLight>(
                new PointLight{ position, color, 1.f, .1f, 0.032f, 0.f } );
}

}


