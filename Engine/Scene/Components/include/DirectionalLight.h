#pragma once

#include <memory>
#include <QVector4D>


namespace ChainX
{
/**
 * @brief Struct for Directional Light component properties
 */
struct DirectionalLight
{
    QVector4D direction;
    QVector4D ambient;
    QVector4D diffuse;
    QVector4D specular;
};

// DirectionalLight struct layout info for GLSL buffer objects
inline constexpr unsigned int sizeof_DirLight = sizeof(DirectionalLight);
inline constexpr unsigned int offsets_DirLight[] =
{
    0, 16, 32, 48
};


/**
 * @brief Small function that create a Directional Light component
 */
inline std::shared_ptr<DirectionalLight> CreateDirLight(
        QVector4D direction,
        QVector4D ambient  = QVector4D(.2f, .2f, .2f, 1.f),
        QVector4D diffuse  = QVector4D(.8f, .8f, .8f, 1.f),
        QVector4D specular = QVector4D(.8f, .8f, .8f, 1.f))
{
    return std::shared_ptr<DirectionalLight>(
                new DirectionalLight{ direction, ambient, diffuse, specular } );
}

}


