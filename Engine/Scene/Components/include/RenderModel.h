#pragma once

#include "Component.h"
#include "BoundingBox.h"
#include <memory>

namespace ChainX
{
class Mesh;
class Material;

class RenderModel : public Component
{
public:
    RenderModel(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material, Entity* entity);

    Mesh* GetMesh() const { return mMesh.get(); }
    Material* GetMaterial() const { return mMaterial.get(); }

    const math::BoundingBox& GetBoundingBox();

private:
    std::shared_ptr<Mesh> mMesh;
    std::shared_ptr<Material> mMaterial;

    math::BoundingBox mBoundingBox;
    math::BoundingBox mAABB; // Axis-Aligned Bounding Box (Transform Bounding Box)
    QMatrix4x4 mPrevTransform;
};
}


