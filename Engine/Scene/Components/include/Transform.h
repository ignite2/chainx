#pragma once

#include <QMatrix4x4>

namespace ChainX
{
class Entity;

class Transform
{
public:

    Transform();

    void SetPosition(const QVector3D& pos);
    void SetScale(const QVector3D& scale);
    /** @brief Sets rotation of this transform to the specified quaternion rotation */
    void SetRotation(const QQuaternion& rotation);
    /**
     * @brief Sets the rotation of this transform from euler angles, axes rotations are in degrees
     * @param xRot : Rotation around the x axis (pitch)
     * @param yRot : Rotation around the y axis (yaw)
     * @param zRot : Rotation around the z axis (roll)
     */
    void SetRotation(float xRot, float yRot, float zRot);

    void Translate(const QVector3D& delta);
    void Rotate(float deltaX, float deltaY, float deltaZ);
    void Rotate(const QQuaternion& delta);

    const QVector3D&   Position() const { return mPosition; };
    const QVector3D&   Scale()    const { return mScale; };
    const QQuaternion& Rotation() const { return mRotation; };

    QVector3D   WorldPosition() const;
    QVector3D   WorldScale()    const;
    QQuaternion WorldRotation() const;

    const QMatrix4x4& GetMatrix();

    void UpdateMatrix();

    void ClearChildren();
    void SetChildren(const std::vector<std::shared_ptr<Entity>>& childEntites);

    void SetParent(Transform* parent);
    void AddChild(Transform* child);

private:
    // Hierarchical model/world transform (pos/rotation/scale)
    QMatrix4x4  mMatrix;

    // Local transform components
    QVector3D   mPosition;
    QVector3D   mScale = QVector3D(1.f, 1.f, 1.f);
    QQuaternion mRotation;

    Transform* mParent = nullptr;
    std::vector<Transform*> mChildren;
};
}


