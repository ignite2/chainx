#pragma once

#include <memory>
#include <vector>

namespace ChainX
{
class Entity;

class Scene
{
public:
    Scene();

    std::shared_ptr<Entity> CreateEntity(Entity* parent = nullptr);

    void DeleteEntity(std::shared_ptr<Entity> entity);

    std::shared_ptr<Entity> GetEntity(std::uint32_t id);
    std::shared_ptr<const Entity> GetRootEntity() { return mRootEntity; };

    std::vector<std::shared_ptr<Entity>> GetAllEntities();

private:
    std::shared_ptr<Entity> mRootEntity;
};
}


