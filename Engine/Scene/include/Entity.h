#pragma once

#include <QMatrix4x4>
#include <QVector3D>
#include <QVector4D>
#include <vector>

namespace ChainX
{
class RenderModel;
class Transform;
struct DirectionalLight;
struct PointLight;

class Entity;
using EntityPtr = std::shared_ptr<Entity>;
using EntitiesVector = std::vector<std::shared_ptr<Entity>>;

class Entity
{
public:
    explicit Entity(Entity* parent = nullptr);

    /// Components
    void SetRenderModel(std::shared_ptr<RenderModel> renderModel);
    void SetTransform(std::shared_ptr<Transform> transform);
    void SetDirLight(std::shared_ptr<DirectionalLight> light);
    void SetPointLight(std::shared_ptr<PointLight> light);

    // Prefer returning raw pointers for performance
    RenderModel* GetRenderModel() const { return mRenderModel.get(); }
    Transform* GetTransform() { return mTransform.get(); };
    DirectionalLight* GetDirLight() { return mDirLight.get(); }
    PointLight* GetPointLight() { return mPointLight.get(); }

    /// Hierarchy
    void AddChild(std::shared_ptr<Entity> entity);
    void RemoveChild(std::shared_ptr<Entity> entity);
    void RemoveAllChildren();

    bool HasChildren() const { return !mChildren.empty(); }
    const EntitiesVector& GetChildren() const { return mChildren; };

    Entity* Parent() const { return mParent; }

    /// Properties
    void SetName(const std::string& name) { mName = name; }
    const std::string& GetName() const    { return mName; }

    std::uint32_t GetId() const { return mId; }

private:
    Entity* mParent;

    std::vector<std::shared_ptr<Entity>> mChildren;

    std::shared_ptr<RenderModel>      mRenderModel;
    std::shared_ptr<Transform>        mTransform;
    std::shared_ptr<DirectionalLight> mDirLight;
    std::shared_ptr<PointLight>       mPointLight;

    std::uint32_t mId;
    std::string mName;
};
}



