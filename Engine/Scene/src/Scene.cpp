#include "Scene.h"
#include "Entity.h"

namespace ChainX
{
namespace
{
std::shared_ptr<Entity> FindEntityDFS(const std::shared_ptr<Entity>& node, std::uint32_t id)
{
    if (node->GetId() == id)
    {
        return node;
    }
    else
    {
        for (const auto& child : node->GetChildren())
        {
            if (auto ent = FindEntityDFS(child, id))
                return ent;
        }
    }
    return nullptr;
}

void InsertEntitiesPreOrderDFS(const std::shared_ptr<Entity>& node,
                               std::vector<std::shared_ptr<Entity>>& outEntities)
{
    if (node->GetChildren().empty())
    {
        outEntities.push_back(node);
    }
    else
    {
        outEntities.push_back(node);
        for (const auto& child : node->GetChildren())
        {
            InsertEntitiesPreOrderDFS(child, outEntities);
        }
    }
}
}


Scene::Scene()
{
    mRootEntity = std::shared_ptr<Entity>(new Entity);
}

std::shared_ptr<Entity> Scene::CreateEntity(Entity* parent)
{
    std::shared_ptr<Entity> new_entity(new Entity);
    if (parent)
    {
        parent->AddChild(new_entity);
    }
    else
    {
        mRootEntity->AddChild(new_entity);
    }

    return new_entity;
}

void Scene::DeleteEntity(std::shared_ptr<Entity> entity)
{
    // Remove this entity from its parent
    auto parent = entity->Parent();
    if (parent != nullptr)
    {
        parent->RemoveChild(entity);
    }
    // Else if root entity
    else if (entity->GetId() == mRootEntity->GetId())
    {
        // mRootEntity.reset(new Entity);
        mRootEntity->RemoveAllChildren();
    }
}

std::shared_ptr<Entity> Scene::GetEntity(std::uint32_t id)
{
    return FindEntityDFS(mRootEntity, id);
}

std::vector<std::shared_ptr<Entity>> Scene::GetAllEntities()
{
    std::vector<std::shared_ptr<Entity>> all_entities;

    InsertEntitiesPreOrderDFS(mRootEntity, all_entities);

    return all_entities;
}
}


