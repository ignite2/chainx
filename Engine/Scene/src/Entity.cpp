#include "Entity.h"
#include "Transform.h"

namespace ChainX
{
namespace
{
inline std::uint32_t CounterId = 0;
}

Entity::Entity(Entity* parent)
    :
      mParent(parent),
      mTransform(new Transform),
      mId(CounterId++)
{
}

void Entity::SetRenderModel(std::shared_ptr<RenderModel> renderModel)
{
    mRenderModel = std::move(renderModel);
}

void Entity::SetTransform(std::shared_ptr<Transform> transform)
{
    mTransform = std::move(transform);
}

void Entity::SetDirLight(std::shared_ptr<DirectionalLight> light)
{
    mDirLight = std::move(light);
}

void Entity::SetPointLight(std::shared_ptr<PointLight> light)
{
    mPointLight = std::move(light);
}

void Entity::AddChild(std::shared_ptr<Entity> entity)
{
    // If new child entity already has a parent, then remove it from its old parent
    if (entity->mParent)
    {
        entity->mParent->RemoveChild(entity);
    }

    entity->mParent = this;
    mChildren.push_back(entity);
}

void Entity::RemoveChild(std::shared_ptr<Entity> entity)
{
    for (auto it = mChildren.begin(); it != mChildren.end(); ++it)
    {
        if ((*it)->GetId() == entity->GetId())
        {
            mChildren.erase(it);
            break;
        }
    }
}

void Entity::RemoveAllChildren()
{
    mChildren.clear();
}
}

