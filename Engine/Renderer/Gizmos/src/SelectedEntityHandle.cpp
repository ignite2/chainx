#include "SelectedEntityHandle.h"
#include "Entity.h"
#include "RenderModel.h"
#include "PositionHandle.h"

namespace ChainX
{
SelectedEntityHandle::SelectedEntityHandle()
{
    mHandles[TransformType::POSITION] = std::make_shared<PositionHandle>();
}

void SelectedEntityHandle::SetSelectedEntity(const std::shared_ptr<Entity>& entity)
{
    mEntity = entity;

    if (entity && entity->GetRenderModel())
    {
        Mesh* mesh = entity->GetRenderModel()->GetMesh();
        // Use vertices from selected entity's mesh
        // The outline mesh needs to have smooth normals for outline
        // calculations. The entity's mesh normals are not always
        // smooth, thus this calculation needs to take place
        mOutlineMesh = std::make_unique<Mesh>(mesh->Vertices());
        if (mesh->Indices().size())
        {
            mOutlineMesh->SetIndices(mesh->Indices());
        }
        mOutlineMesh->CalculateSmoothNormals();
    }
    else
    {
        mOutlineMesh.reset();
    }
}
}

