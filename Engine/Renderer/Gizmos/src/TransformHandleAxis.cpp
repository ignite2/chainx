#include "TransformHandleAxis.h"

namespace ChainX
{
TransformHandleAxis::TransformHandleAxis(const QVector3D& color, const QVector3D& axis)
    :
      mColor(color),
      mAxis(axis)
{
}

void TransformHandleAxis::SetPosition(const QVector3D &pos)
{
    mPosition = pos;
    UpdateTransform();
}

void TransformHandleAxis::SetScale(const QVector3D &scale)
{
    mScale = scale;
    UpdateTransform();
}

void TransformHandleAxis::SetRotation(const QQuaternion &rotation)
{
    mRotation = rotation;
    UpdateTransform();
}

void TransformHandleAxis::UpdateTransform()
{
    mTransformMatrix.setToIdentity();
    mTransformMatrix.translate(mPosition);
    mTransformMatrix.rotate(mRotation);
    mTransformMatrix.scale(mScale);
}

void TransformHandleAxis::SetAttachLink(const QVector3D& handleCenter)
{
    if (!mPosColorBuffer.isCreated())
        mPosColorBuffer.create();

    mPosColorBuffer.bind();
    mPosColorBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);

    auto stride = 2 * 3 * sizeof(float); // Stride is for position + color
    std::vector<QVector3D> vertices;

    // TODO: From handle position to center
    // pos
    vertices.push_back(mColor);
    // center
    vertices.push_back(mColor);

    auto data_size = static_cast<int>(vertices.size());
    mPosColorBuffer.allocate(vertices.data(), 3* data_size * sizeof(float));

    mPosColorBuffer.release();
}

}
