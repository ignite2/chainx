#include "Grid.h"
#include "QOpenGLShaderProgram"

namespace ChainX
{
Grid::Grid(const std::uint32_t length)
    : mColor{0.f, 0.f, 0.f},
      mModel{},
      mGridMesh{new Mesh}
{
    mGridMesh->SetTopology(Topology::LINES);
    BuildGrid(length);
}

void Grid::BuildGrid(const std::uint32_t length)
{
    std::vector<QVector3D> vertices;

    int half_len = length / 2;

    for (auto i = -half_len; i <= half_len; ++i)
    {
        vertices.push_back({-static_cast<float>(half_len), 0.f, static_cast<float>(i)});
        vertices.push_back({ static_cast<float>(half_len), 0.f, static_cast<float>(i)});
        vertices.push_back({ static_cast<float>(i), 0.f, -static_cast<float>(half_len)});
        vertices.push_back({ static_cast<float>(i), 0.f,  static_cast<float>(half_len)});
    }

    mGridMesh->SetVertices(std::move(vertices));
}

void Grid::SetShader(std::shared_ptr<QOpenGLShaderProgram> gridShader)
{
    mShader = std::move(gridShader);

    mGridMesh->Finalize(mShader.get());
}

void Grid::SetGridColor(QVector3D color)
{
    mColor = std::move(color);
}

void Grid::SetParameters()
{
    mShader->setUniformValue("color", mColor);
    mShader->setUniformValue("model", mModel);
}
}

