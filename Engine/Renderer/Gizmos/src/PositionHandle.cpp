#include "PositionHandle.h"
#include "mesh_shapes.h"

namespace ChainX
{
PositionHandle::PositionHandle()
    :
      TransformHandle(TransformType::POSITION)
{
    mHandleMesh = utils::shapes::CreatePyramid();
//    mHandleMesh->Finalize(mShader.get());

    // TODO: Create bounding boxes to grab handles
}
}

