#include "TransformHandle.h"
#include "Entity.h"
#include "RenderModel.h"

namespace ChainX
{
TransformHandle::TransformHandle(TransformType type)
    :
      mHandleX({1.f, 0.f, 0.f}, {1.f, 0.f, 0.f}),
      mHandleY({0.f, 1.f, 0.f}, {0.f, 1.f, 0.f}),
      mHandleZ({0.f, 0.f, 1.f}, {0.f, 0.f, 1.f}),
      mType(type)
{
    // Set axes rotation
    mHandleX.SetRotation(QQuaternion::fromEulerAngles( 0.f,  0.f, -90.f));
    mHandleY.SetRotation(QQuaternion::fromEulerAngles( 0.f, 90.f,   0.f));
    mHandleZ.SetRotation(QQuaternion::fromEulerAngles(90.f,  0.f,   0.f));
}

void TransformHandle::Update(Entity *entity)
{
    RenderModel* ent_model = entity->GetRenderModel();

    QVector3D center = ent_model->GetBoundingBox().GetCenter();
    float handle_length = 1; // TODO: Determine length by camera distance?

    // Handle positions
    mHandleX.SetPosition( center + mHandleX.GetAxis() * handle_length );
    mHandleY.SetPosition( center + mHandleY.GetAxis() * handle_length );
    mHandleZ.SetPosition( center + mHandleZ.GetAxis() * handle_length );

    // Handle scales
    QVector3D handle_size{0.25, 0.25, 0.25}; // TODO: Determine size by camera distance
    mHandleX.SetScale(handle_size);
    mHandleY.SetScale(handle_size);
    mHandleZ.SetScale(handle_size);
}

void TransformHandle::SetShader(std::shared_ptr<QOpenGLShaderProgram> shader)
{
    mShader = std::move(shader);

    mHandleMesh->Finalize(mShader.get());
}

QMatrix4x4 TransformHandle::GetTransform(TransformAxis axis)
{
    if (axis == TransformAxis::X)
        return mHandleX.GetTransform();
    else if (axis == TransformAxis::Y)
        return mHandleY.GetTransform();
    else if (axis == TransformAxis::Z)
        return mHandleZ.GetTransform();
    else
        return QMatrix4x4();
}

const Mesh& TransformHandle::GetHandleMesh(TransformAxis axis)
{
    return *mHandleMesh;
}

QVector3D TransformHandle::GetColor(TransformAxis axis)
{
    if (axis == TransformAxis::X)
        return mHandleX.GetColor();
    else if (axis == TransformAxis::Y)
        return mHandleY.GetColor();
    else if (axis == TransformAxis::Z)
        return mHandleZ.GetColor();
    else
        return {};
}

}

