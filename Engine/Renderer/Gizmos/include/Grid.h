#pragma once

#include "Mesh.h"
#include <QVector3D>
#include <QMatrix4x4>

namespace ChainX
{
class Grid
{
public:
    Grid(const std::uint32_t length = 40);

    void BuildGrid(const std::uint32_t length);

    void SetShader(std::shared_ptr<QOpenGLShaderProgram> gridShader);
    void SetGridColor(QVector3D color);
    void SetParameters();

    const std::shared_ptr<QOpenGLShaderProgram>& Shader() const { return mShader; }

    const Mesh& MeshRef() const { return *mGridMesh; }

private:
    QVector3D  mColor;
    QMatrix4x4 mModel;
    std::unique_ptr<Mesh> mGridMesh;
    std::shared_ptr<QOpenGLShaderProgram> mShader;
};
}


