#pragma once

#include "BoundingBox.h"
#include "Mesh.h"
#include <QVector3D>

namespace ChainX
{
class TransformHandleAxis
{
public:
    TransformHandleAxis(const QVector3D& color, const QVector3D& axis);

    void SetPosition(const QVector3D& pos);
    void SetScale(const QVector3D& scale);
    void SetRotation(const QQuaternion& rotation);
    void UpdateTransform();

    void SetAttachLink(const QVector3D& handleCenter);

    QMatrix4x4 GetTransform() const { return mTransformMatrix; }

    const QVector3D& GetColor() const { return mColor; }
    const QVector3D& GetAxis()  const { return mAxis; }
    const QOpenGLBuffer& GetAttachLink()  const { return mPosColorBuffer; }

private:
    // Properties
    QVector3D mColor;
    QVector3D mAxis;
    math::BoundingBox mBox;
    // Axis Attachment Link
    std::unique_ptr<Mesh> mAxisAttachmentMesh; // TODO: Remove
    QOpenGLBuffer mPosColorBuffer; // Buffer for Position & Color
    // Transform
    QMatrix4x4  mTransformMatrix;
    QVector3D   mPosition;
    QVector3D   mScale = QVector3D(1.f, 1.f, 1.f);
    QQuaternion mRotation;
};
}


