#pragma once

#include "Mesh.h"
#include "TransformHandleAxis.h"
#include <memory>

namespace ChainX
{
// Forward Declarations
class Entity;

// Helper Types/structs
enum class TransformType
{
    NONE,
    POSITION,
    ROTATION,
    SCALE
};

enum class TransformAxis
{
    X,
    Y,
    Z
};

class TransformHandle
{
public:
    TransformHandle(TransformType type);
    virtual ~TransformHandle() = default;

    virtual void MouseDelta() {}; // TODO
    virtual void Update(Entity* entity);

    // TODO
    void SetShader(std::shared_ptr<QOpenGLShaderProgram> shader);
    void SetParameters();
    QMatrix4x4 GetTransform(TransformAxis axis);
    const Mesh& GetHandleMesh(TransformAxis axis);
    const Mesh& GetAxisMesh(TransformAxis axis);
    QVector3D GetColor(TransformAxis axis);
//    const QVector3D& GetColor(TransformAxis axis);

protected:
    TransformHandleAxis mHandleX;
    TransformHandleAxis mHandleY;
    TransformHandleAxis mHandleZ;
    TransformType mType;

    std::shared_ptr<Mesh> mHandleMesh;
    std::shared_ptr<QOpenGLShaderProgram> mShader;
};
}


