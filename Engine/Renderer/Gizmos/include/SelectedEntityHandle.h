#pragma once

#include "Mesh.h"
#include "TransformHandle.h"
#include <memory>

namespace ChainX
{
// Forward Declarations
class Entity;

class SelectedEntityHandle
{
public:
    SelectedEntityHandle();

    void SetSelectedEntity(const std::shared_ptr<Entity>& entity);
    std::shared_ptr<Entity> SelectedEntity() { return mEntity.lock(); };

    Mesh* OutlineMesh() { return mOutlineMesh.get(); };
    TransformHandle* GetTransformHandle() { return mHandles[mActiveType].get(); }

private:
    std::weak_ptr<Entity> mEntity;

    // Outline attributes
    std::unique_ptr<Mesh> mOutlineMesh;

    // TODO: Add transform tools to move/scale/rotate selected entity
    // TODO:
    // Transform Handles
    TransformType mActiveType = TransformType::POSITION;
    std::unordered_map<TransformType, std::shared_ptr<TransformHandle>> mHandles;
    // TODO: Finish
};
}


