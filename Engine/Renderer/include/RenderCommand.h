#pragma once

#include "Material.h"
#include "Mesh.h"
#include <QMatrix4x4>

namespace ChainX
{
struct RenderCommand
{
    Mesh*      mesh;
    Material*  material;
    QMatrix4x4 model_transform;
};
}
