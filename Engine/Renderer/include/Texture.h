#pragma once

#include "glad/glad.h"

#include <string>

namespace ChainX
{
class Texture
{
public:
    explicit Texture(std::string filename, GLenum textureTarget = GL_TEXTURE_2D);

    void Generate(GLenum colorFormat, bool useMipmap = false);

    void Bind(GLenum textureUnit);
    void Unbind();

private:
    std::string mFilename;
public:
    GLuint mTexID;
    GLenum mTexTarget;
};
}


