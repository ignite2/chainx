#ifndef MESH_H
#define MESH_H

#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QVector2D>
#include <QVector3D>
#include <memory>
#include <vector>


class QOpenGLShaderProgram;

namespace ChainX
{
enum class Topology
{
    LINES,
    TRIANGLES,
    TRIANGLE_STRIP,
};

class Mesh
{
public:

    Mesh();

    Mesh(std::vector<QVector3D> vertices);
    Mesh(std::vector<QVector3D> vertices, std::vector<std::uint32_t> indices);
    Mesh(std::vector<QVector3D> vertices, std::vector<QVector3D> normals);
    Mesh(std::vector<QVector3D> vertices, std::vector<QVector3D> normals,
         std::vector<std::uint32_t> indices);
    Mesh(std::vector<QVector3D> vertices, std::vector<QVector3D> normals,
         std::vector<QVector2D> uvCoords, std::vector<std::uint32_t> indices);

    void SetVertices(std::vector<QVector3D> vertices) { mVertices = std::move(vertices); };
    void SetIndices(std::vector<std::uint32_t> indices) { mIndices = std::move(indices); };
    void SetUVs(std::vector<QVector2D> uvCoords) { mUV = std::move(uvCoords); };

    const std::vector<QVector3D>&   Vertices() const { return mVertices; }
    const std::vector<unsigned int>& Indices() const { return mIndices; }

    void SetTopology(const Topology mode);
    GLenum GLTopology() const;

    QOpenGLVertexArrayObject* VertexArrayBuf() const { return mVao.get(); }

    void CalculateNormals();
    void CalculateSmoothNormals();
    void Finalize(QOpenGLShaderProgram* shader);

    std::uint32_t NumTriangles();

private:
    std::unique_ptr<QOpenGLVertexArrayObject> mVao;
    QOpenGLBuffer mVbo;
    QOpenGLBuffer mEbo;

    std::vector<QVector3D> mVertices;
    std::vector<QVector3D> mNormals;
    std::vector<QVector2D> mUV;

    std::vector<std::uint32_t> mIndices;

    GLenum mTopology;
};
}
#endif // MESH_H
