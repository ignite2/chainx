#ifndef CAMERA_H
#define CAMERA_H

#include <QMatrix4x4>
#include <QVector3D>

namespace ChainX
{
class Entity;

class Camera
{
public:
    Camera();

    void SetFov(float fov);
    void SetAspect(float aspect);
    void SetPerspective(const float fov, const float aspect, const float near, const float far);

    float Fov() const { return mFov; };

    void SetFocusPoint(const QVector3D& vertex);

    void MouseRotate(float deltaX, float deltaY);
    void MousePan(float deltaX, float deltaY);
    void MouseVPan(float deltaX, float deltaY);
    void MouseZoom(float delta);
    // TODO: Use application context and remove height and width viewport params
    std::shared_ptr<Entity> MousePick(float posX, float posY,
                                      const std::vector<std::shared_ptr<Entity>>& entities,
                                      float width, float height);

    void SetMouseSensitivity(float sensitivity) { mMouseSensitivity = sensitivity; };
    float MouseSensitivity() const { return mMouseSensitivity; };
    void SetZoomSensitivity(float sensitivity)  { mZoomSensitivity = sensitivity; };
    float ZoomSensitivity() const { return mZoomSensitivity; };

    const QVector3D& Position() const { return mPosition; };

    const QMatrix4x4& ViewMatrix() const { return mView; };
    const QMatrix4x4& ProjMatrix() const { return mProjection; };

private:
    void RotateAroundVertex(const float deltaAz, const float deltaEl, const QVector3D& vertex);
    void UpdateUpVector();
    void UpdateView();
    void UpdateProjection();


    float mFov;
    float mAspect;
    float mNear;
    float mFar;

    QVector3D mPosition   { 0.f, 3.f, 12.f };
    QVector3D mDirection  { 0.f, 0.f, -1.f };
    QVector3D mUp         { 0.f, 1.f,  0.f };
    QVector3D mWorldUp    { 0.f, 1.f,  0.f };
    QVector3D mFocusVertex{ 0.f, 1.f,  0.f };

    QMatrix4x4 mView;
    QMatrix4x4 mProjection;

    float mMouseSensitivity = 0.01f;
    float mZoomSensitivity  = 2.0f;

};
}

#endif // CAMERA_H
