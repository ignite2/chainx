#ifndef RENDERER_H
#define RENDERER_H

#include "GLState.h"
#include "RenderCommand.h"
#include "ShaderLibrary.h"
#include "Grid.h"
#include "SelectedEntityHandle.h"

namespace ChainX
{
class Camera;
class Entity;

class Renderer
{
public:

    Renderer();

    void Init();

    void SetRenderItems(std::vector<std::shared_ptr<Entity>> entities, Camera* camera);
    void SetRenderSize(unsigned int width, unsigned int height);

    /** @brief Snaps renderer and sets handle to entity */
    void SetHandleToEntity(const std::shared_ptr<Entity>& entity);

    void ShowGrid(bool show);
    void ShowMeshOutlines(bool show);

    void RenderFrame();

    const ShaderLibrary& GetShaderLib() const { return mShaderLibrary; }

private:

    void DrawModels();
    void DrawOutlines();
    void DrawTransformHandle();

    void ExecRenderCommand(const RenderCommand& cmd);

    void RenderMesh(const Mesh& mesh);

    void UpdateUniformBuffers();

    // Auxiliary Gizmos, Gadgets, Widgets?
    bool mDrawGrid = false;
    Grid mGrid;
    SelectedEntityHandle mEntityHandle; // Handle to selected entity and its transform tool

    // Buffers
    unsigned int mMatricesUBO;
    unsigned int mSceneUBO;

    // Resources
    ChainX::ShaderLibrary mShaderLibrary;

    // Entities
    std::vector<std::shared_ptr<Entity>> mRenderEntities;
    std::vector<std::shared_ptr<Entity>> mDirLightEntities;
    std::vector<std::shared_ptr<Entity>> mPointLightEntities;
    int mNumDirLights;
    int mNumPointLights;

    Camera* mCamera = nullptr;

    // Renderer State
    GLState mGLState;
    unsigned int mWidth;
    unsigned int mHeight;
};
}

#endif // RENDERER_H
