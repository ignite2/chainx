#pragma once

#include "shader_utils.h"
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>


class QOpenGLShaderProgram;

namespace ChainX
{
class ShaderLibrary
{
public:
    ShaderLibrary();

    void Init();

    std::shared_ptr<QOpenGLShaderProgram> GetShader(ShaderType shaderType) const;

    std::vector<std::string> GetLoadedShaders();

private:
    std::unordered_map<ShaderType, std::shared_ptr<QOpenGLShaderProgram>> mShaders;

};

}


