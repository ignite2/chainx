#pragma once

#include "shader_utils.h"
#include <QVector3D>

class QOpenGLShaderProgram;

namespace ChainX
{
class Texture;

template<typename InputType>
struct MaterialInput
{
    // Index
    // IsConnected
    // Connect
    std::string name;
    InputType value;
};
//struct ColorMaterialInput : MaterialInput<QVector3D> {};
// Material inputs for a node graph?


class Material
{
public:

    Material(std::shared_ptr<QOpenGLShaderProgram> shader, ShaderType shaderType = ShaderType::GPU_DEFAULT);

    QOpenGLShaderProgram* Shader() const;

    void SetParameters();

    void SetTexture(std::shared_ptr<Texture> texture);
    auto GetTexture() const { return mTexture; };

    void SetBaseColor(QVector3D color);
    const QVector3D& BaseColor() const { return mBaseColor; };
    void SetSpecularColor(QVector3D specColor);
    const QVector3D& SpecularColor() const { return mSpecular; };

    void SetSpecularPower(float power);
    float SpecularPower() const { return mSpecPower; };

    void SetSpecularStrength(float strength);
    float SpecularStrength() const { return mSpecStrength; };

private:
    std::shared_ptr<QOpenGLShaderProgram> mShader;
    ShaderType mShaderType;

    // Properties
    std::shared_ptr<Texture> mTexture;
    QVector3D mBaseColor {.8f, .8f, .8f};
    QVector3D mSpecular  {.5f, .5f, .5f};
    float     mSpecPower = 8.f; // Shininess
    float     mSpecStrength = 0.5f;
};


}


