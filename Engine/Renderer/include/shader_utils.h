#pragma once

#include <memory>
#include <string>
#include <QOpenGLShaderProgram>

using ShaderProgram = QOpenGLShaderProgram;

namespace ChainX
{
enum ShaderType
{
    GPU_DEFAULT,
    GPU_UNIFORM_COLOR,
    GPU_OUTLINE,
};

std::shared_ptr<QOpenGLShaderProgram>
LoadShaderProgram(std::string_view vertShaderFilename,
                  std::string_view fragShaderFilename);

std::string ShaderTypeToString(ShaderType type);
}


