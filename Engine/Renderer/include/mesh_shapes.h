#pragma once

#include <memory>

namespace ChainX
{
class Mesh;

namespace utils::shapes
{
std::shared_ptr<Mesh> CreatePlane();

std::shared_ptr<Mesh> CreateTriangle();

std::shared_ptr<Mesh> CreateCircle(unsigned int slices = 360);

std::shared_ptr<Mesh> CreatePyramid();

std::shared_ptr<Mesh> CreateCube();

std::shared_ptr<Mesh> CreateSphere();

std::shared_ptr<Mesh> CreateCylinder(float radiusTop = 1, float radiusBottom = 1);
}
}
