#pragma once


// Typedef GL types instead of including gl lib to save compile time.
// C++ standard allows for multiple identical typedefs
typedef unsigned int GLenum;
typedef unsigned int GLuint;
typedef int          GLint;
//#include <glad/glad.h>

namespace ChainX
{
/**
 * @brief GLState class to store the current OpenGL state
 */
class GLState
{
public:
    GLState();

    void EnableDepthTest(bool enable = true);
    void EnableCullFace(bool enable = true);
    void EnableStencilTest(bool enable = true);

    void SetDepthFunction(GLenum depthFunc);
    void SetFrontFaceWindOrder(GLenum windOrder);

    void SetPolygonMode(GLenum mode);

    void WriteToStencil(bool write);
    void SetStencilFunction(GLenum stencilFunc, GLint ref, GLuint mask = 0xFF);
    void ResetStencil();

    /**
     * @brief Applies the stored OpenGL state of this instance to an OpenGL context
     * Notes:
     *   Stencil buffer state is reset
     */
    void ApplyState();

    bool IsStencilEnable() const { return mStencilTest; };

private:
    // Enable/Disable State Capabilities
    bool mDepthTest;
    bool mCullFace;
    bool mStencilTest;

    // State Attributes/Behavior
    GLenum mDepthFunc;
    GLenum mWindOrder;
    GLenum mPolygonMode;
    // Stencil Attributes
    GLenum mStencilFunc;
    GLint  mStencilRef;
    GLuint mStencilMask;
    GLuint mStencilWriteMask;
};
}



