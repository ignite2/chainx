#include "shader_utils.h"
#include "Logger.h"

namespace ChainX
{
std::shared_ptr<QOpenGLShaderProgram>
LoadShaderProgram(std::string_view vertShaderFilename,
                  std::string_view fragShaderFilename)
{
    std::shared_ptr<QOpenGLShaderProgram> shader{new QOpenGLShaderProgram};

    if ( !shader->addShaderFromSourceFile(QOpenGLShader::Vertex, vertShaderFilename.data()) )
    {
        Logger::LogError("GLSL Compile Error {}\n{}", vertShaderFilename,
                         shader->log().toLocal8Bit().constData());
    }
    if ( !shader->addShaderFromSourceFile(QOpenGLShader::Fragment, fragShaderFilename.data()) )
    {
        Logger::LogError("GLSL Compile Error {}\n{}", fragShaderFilename,
                         shader->log().toLocal8Bit().constData());
    }

    if (!shader->link())
    {
        Logger::LogError("Error Linking Shader Programs!\n  {}\n  {}\n{}",
                         vertShaderFilename, fragShaderFilename,
                         shader->log().toLocal8Bit().constData());
    }

    return shader;
}

std::string ShaderTypeToString(ShaderType type)
{
    switch(type)
    {
    case ShaderType::GPU_DEFAULT:
        return "Default";
    case ShaderType::GPU_UNIFORM_COLOR:
        return "Uniform Color";
    default:
        return "UNDEFINED";
    }
}
}
