#include "ShaderLibrary.h"


namespace ChainX
{
ShaderLibrary::ShaderLibrary()
{
}

void ShaderLibrary::Init()
{
    mShaders[ShaderType::GPU_DEFAULT] = LoadShaderProgram(":/shaders/forward_render_vert",
                                                          ":/shaders/forward_render_frag");
    mShaders[ShaderType::GPU_UNIFORM_COLOR] = LoadShaderProgram(":/shaders/vert_shader",
                                                                ":/shaders/uniform_color_frag");
    mShaders[ShaderType::GPU_OUTLINE] = LoadShaderProgram(":/shaders/outline_vert",
                                                          ":/shaders/outline_frag");
}

std::shared_ptr<QOpenGLShaderProgram> ShaderLibrary::GetShader(ShaderType shaderType) const
{
    auto it = mShaders.find(shaderType);
    if (it != mShaders.end())
    {
        return it->second;
    }
    else
    {
        return nullptr;
    }
}

std::vector<std::string> ShaderLibrary::GetLoadedShaders()
{
    std::vector<std::string> shader_names;
    for (auto& [type, shader] : mShaders)
    {
        shader_names.push_back(ShaderTypeToString(type));
    }
    return shader_names;
}
}
