#include "Material.h"


namespace ChainX
{
Material::Material(std::shared_ptr<QOpenGLShaderProgram> shader, ShaderType shaderType)
    :
      mShader(std::move(shader)), mShaderType(shaderType)
{
}

QOpenGLShaderProgram* Material::Shader() const
{
    return mShader.get();
}

void Material::SetParameters()
{
    if (mShaderType == ShaderType::GPU_DEFAULT)
    {
        mShader->setUniformValue("material.ambient", mBaseColor);
        mShader->setUniformValue("material.diffuse", mBaseColor);
        mShader->setUniformValue("material.specular", mSpecular);
        mShader->setUniformValue("material.specularPower", mSpecPower);
        mShader->setUniformValue("material.specularStrength", mSpecStrength);
    }
    else if (mShaderType == ShaderType::GPU_UNIFORM_COLOR)
    {
        mShader->setUniformValue("color", mBaseColor);
    }
}

void Material::SetTexture(std::shared_ptr<Texture> texture)
{
    mTexture = std::move(texture);
}

void Material::SetBaseColor(QVector3D color)
{
    mBaseColor = std::move(color);
}

void Material::SetSpecularColor(QVector3D specColor)
{
    mSpecular = std::move(specColor);
}

void Material::SetSpecularPower(float power)
{
    mSpecPower = power;
}

void Material::SetSpecularStrength(float strength)
{
    mSpecStrength = strength;
}
}


