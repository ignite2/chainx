#include "mesh_shapes.h"
#include "constants.h"
#include "Mesh.h"

#include <vector>
#include <cstdint>

namespace ChainX::utils::shapes
{
std::shared_ptr<Mesh> CreatePlane()
{
    int x_segments = 2;
    int z_segments = 2;

    std::vector<QVector3D> vertices;
    std::vector<QVector3D> normals;
    for (auto x = 0; x <= x_segments; ++x)
    {
        for (auto z = 0; z <= z_segments; ++z)
        {
            vertices.emplace_back(x, 0, z);
            normals.emplace_back(0, 1, 0);
        }
    }

    std::vector<std::uint32_t> indices;
    for (auto x = 0; x < x_segments; ++x)
    {
        for (auto z = 0; z < z_segments; ++z)
        {
            auto col1 = x * (z_segments+1);
            auto col2 = (x+1) * (z_segments+1);
            // triangle 1
            indices.push_back(col1 + z);
            indices.push_back(col1 + z + 1);
            indices.push_back(col2 + z);
            // triangle 2
            indices.push_back(col1 + z + 1);
            indices.push_back(col2 + z + 1);
            indices.push_back(col2 + z);
        }
    }

    std::shared_ptr<Mesh> mesh( new Mesh(std::move(vertices), std::move(normals), std::move(indices)) );

    return mesh;
}

std::shared_ptr<Mesh> CreateTriangle()
{
    std::vector<QVector3D> vertices =
    {
        {-0.5f, -0.5f, 0.0f},
        { 0.5f, -0.5f, 0.0f},
        { 0.f,   0.5f, 0.0f}
    };
    std::vector<QVector2D> uv =
    {
        {0.0f, 0.0f },
        {1.0f, 0.0f },
        {0.5f, 1.0f },
    };

    std::shared_ptr<Mesh> mesh( new Mesh(std::move(vertices)) );
    mesh->SetUVs(uv);

    return mesh;
}

std::shared_ptr<Mesh> CreateCircle(unsigned int slices)
{
    unsigned int step = 1;
    if (slices > 0 && 360u % slices == 0)
        step = 360u / slices;

    std::vector<QVector3D> vertices;
    for (auto i = 0u; i <= 360u; i+=step)
    {
        float angle = static_cast<float>(i)  * math::constants::deg_to_rad;
        float x_pos = std::cos(angle);
        float y_pos = std::sin(angle);

        vertices.emplace_back(x_pos, y_pos, 0.f);
    }
    vertices.emplace_back(0.f, 0.f, 0.f);
    int center_idx = static_cast<int>(vertices.size()) - 1;

    std::vector<std::uint32_t> indices;
    for (auto i = 0u; i <= slices; ++i)
    {
        indices.push_back(i);
        indices.push_back(center_idx);
    }

    std::shared_ptr<Mesh> mesh( new Mesh(std::move(vertices), std::move(indices)) );
    mesh->SetTopology(Topology::TRIANGLE_STRIP);

    return mesh;
}

std::shared_ptr<Mesh> CreatePyramid()
{
    std::vector<QVector3D> vertices =
    {
        // base
        {-0.5f, -0.5f, -0.5f},
        { 0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f,  0.5f},
        {-0.5f, -0.5f,  0.5f},
        { 0.5f, -0.5f, -0.5f},
        { 0.5f, -0.5f,  0.5f},
        // Back
        { 0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f, -0.5f},
        { 0.0f,  0.5f,  0.0f},
        // Right
        { 0.5f, -0.5f, -0.5f},
        { 0.0f,  0.5f,  0.0f},
        { 0.5f, -0.5f,  0.5f},
        // Front
        {-0.5f, -0.5f, 0.5f},
        { 0.5f, -0.5f, 0.5f},
        { 0.0f,  0.5f, 0.0f},
        // Left
        {-0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f,  0.5f},
        { 0.0f,  0.5f,  0.0f},
    };

    std::shared_ptr<Mesh> mesh( new Mesh(std::move(vertices)) );
    mesh->CalculateNormals();

    return mesh;
}

std::shared_ptr<Mesh> CreateCube()
{
    std::vector<QVector3D> vertices =
    {
        // Back
        { 0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f, -0.5f},
        { 0.5f,  0.5f, -0.5f},
        {-0.5f,  0.5f, -0.5f},
        { 0.5f,  0.5f, -0.5f},
        {-0.5f, -0.5f, -0.5f},
        // Front
        {-0.5f, -0.5f,  0.5f},
        { 0.5f, -0.5f,  0.5f},
        { 0.5f,  0.5f,  0.5f},
        { 0.5f,  0.5f,  0.5f},
        {-0.5f,  0.5f,  0.5f},
        {-0.5f, -0.5f,  0.5f},
        // Left
        {-0.5f,  0.5f,  0.5f},
        {-0.5f,  0.5f, -0.5f},
        {-0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f,  0.5f},
        {-0.5f,  0.5f,  0.5f},
        // Right
        {0.5f,  0.5f, -0.5f},
        {0.5f,  0.5f,  0.5f},
        {0.5f, -0.5f, -0.5f},
        {0.5f, -0.5f,  0.5f},
        {0.5f, -0.5f, -0.5f},
        {0.5f,  0.5f,  0.5f},
        // Base
        {-0.5f, -0.5f, -0.5f},
        { 0.5f, -0.5f, -0.5f},
        { 0.5f, -0.5f,  0.5f},
        { 0.5f, -0.5f,  0.5f},
        {-0.5f, -0.5f,  0.5f},
        {-0.5f, -0.5f, -0.5f},
        // Top
        { 0.5f,  0.5f, -0.5f },
        {-0.5f,  0.5f, -0.5f},
        { 0.5f,  0.5f,  0.5f},
        {-0.5f,  0.5f,  0.5f},
        { 0.5f,  0.5f,  0.5f},
        {-0.5f,  0.5f, -0.5f},
    };

    std::shared_ptr<Mesh> mesh( new Mesh(std::move(vertices)) );
    mesh->CalculateNormals();

    return mesh;
}

std::shared_ptr<Mesh> CreateSphere()
{
    const auto arc_length = 360u; // maximum length of an arc or max degrees in a circle
    const unsigned int half_arc_length = arc_length * .5; // 180

    // Calculare vertices and normals
    std::vector<QVector3D> vertices;
    std::vector<QVector3D> normals;
    for (auto x_idx = 0u; x_idx <= arc_length; ++x_idx)
    {
        auto az = static_cast<float>(x_idx) * math::constants::deg_to_rad;

        for (auto y_idx = 0u; y_idx <= half_arc_length; ++y_idx)
        {
            auto el = static_cast<float>(y_idx) * math::constants::deg_to_rad;

            float x_pos = std::cos(az) * std::sin(el);
            float y_pos = std::cos(el);
            float z_pos = std::sin(az) * std::sin(el);

            vertices.emplace_back(x_pos, y_pos, z_pos);
            normals.emplace_back(x_pos, y_pos, z_pos);
        }
    }

    // Calculate indices for triangles based on the sphere coordinates
    std::vector<std::uint32_t> indices;
    for (auto i = 0u; i < arc_length; ++i)
    {
        for (auto j = 0u; j < half_arc_length; ++j)
        {
            auto col1 = i * (half_arc_length+1);
            auto col2 = (i+1) * (half_arc_length+1);
            // triangle 1
            indices.push_back(col1 + j);
            indices.push_back(col1 + j + 1);
            indices.push_back(col2 + j);
            // triangle 2
            indices.push_back(col1 + j + 1);
            indices.push_back(col2 + j + 1);
            indices.push_back(col2 + j);
        }
    }

    std::shared_ptr<Mesh> mesh( new Mesh(std::move(vertices),
            std::move(normals), std::move(indices)) );

    return mesh;
}

std::shared_ptr<Mesh> CreateCylinder(float radiusTop, float radiusBottom)
{
    const auto slices = 360u; // slices = arc length of a circle in degrees
    const auto rings = 2u;
    const float height = 1.f;
    const float ringHeight = height / (rings-1);
    const float rad_step = (radiusTop - radiusBottom) / (rings-1);

    std::vector<std::uint32_t> indices;
    std::vector<QVector3D> vertices;
    std::vector<QVector3D> normals;
    // Cylinder body
    for (auto i = 0u; i <= slices; ++i)
    {
        float angle = static_cast<float>(i) * math::constants::deg_to_rad;
        float x = std::cos(angle);
        float z = std::sin(angle);

        for (auto j = 0u; j < rings; ++j)
        {
            float y = -.5f*height + j * ringHeight;
            float r = radiusBottom + j*rad_step;

            vertices.emplace_back(r * x, y, r * z);
            normals.emplace_back(x, 0, z);
        }
    }

    for (auto i = 0u; i < slices; ++i)
    {
        indices.push_back( i*rings );
        indices.push_back( i*rings + 1 );
        indices.push_back( (i+1)*rings );

        indices.push_back( i*rings + 1 );
        indices.push_back( (i+1)*rings + 1 );
        indices.push_back( (i+1)*rings );
    }

    // Top cap
    int base_idx = static_cast<int>(vertices.size());
    float y = height * .5f;
    for (auto i = 0u; i <= slices; ++i)
    {
        float angle = static_cast<float>(i) * math::constants::deg_to_rad;
        float x = std::cos(angle);
        float z = std::sin(angle);

        vertices.emplace_back(radiusTop * x, y, radiusTop * z);
        normals.emplace_back(0, 1, 0);
    }
    vertices.emplace_back(0, y, 0);
    normals.emplace_back(0, 1, 0);
    int center_idx = static_cast<int>(vertices.size()) - 1;
    for (auto i = 0u; i < slices; ++i)
    {
        indices.push_back(center_idx);
        indices.push_back(base_idx + i + 1);
        indices.push_back(base_idx + i);
    }

    // Bottom cap
    base_idx = static_cast<int>(vertices.size());
    y = -height * .5f;
    for (auto i = 0u; i <= slices; ++i)
    {
        float angle = static_cast<float>(i) * math::constants::deg_to_rad;
        float x = std::cos(angle);
        float z = std::sin(angle);

        vertices.emplace_back(radiusBottom * x, y, radiusBottom * z);
        normals.emplace_back(0, -1, 0);
    }
    vertices.emplace_back(0, y, 0);
    normals.emplace_back(0, -1, 0);
    center_idx = static_cast<int>(vertices.size()) - 1;
    for (auto i = 0u; i < slices; ++i)
    {
        indices.push_back(center_idx);
        indices.push_back(base_idx + i);
        indices.push_back(base_idx + i + 1);
    }

    std::shared_ptr<Mesh> mesh( new Mesh(std::move(vertices), std::move(normals), std::move(indices)) );

    return mesh;
}
}
