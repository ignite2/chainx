#include "Camera.h"
#include "Ray.h"
#include "Entity.h"
#include "constants.h"

#include <cmath>


namespace
{
}

namespace ChainX
{
Camera::Camera()
{
}

void Camera::SetFov(float fov)
{
    mFov = fov;
    mProjection.setToIdentity();
    mProjection.perspective(mFov, mAspect, mNear, mFar);
}

void Camera::SetAspect(float aspect)
{
    mAspect = aspect;
    mProjection.setToIdentity();
    mProjection.perspective(mFov, mAspect, mNear, mFar);
}

void Camera::SetPerspective(const float fov, const float aspect, const float near, const float far)
{
    mFov = fov;
    mAspect = aspect;
    mNear = near;
    mFar  = far;
    mProjection.setToIdentity();
    mProjection.perspective(mFov, mAspect, mNear, mFar);
}

void Camera::SetFocusPoint(const QVector3D &vertex)
{
    mFocusVertex = vertex;

    mDirection = mFocusVertex - mPosition;
    mDirection.normalize();

    UpdateUpVector();

    mView.setToIdentity();
    mView.lookAt(mPosition, mFocusVertex, mUp);
}

void Camera::MouseRotate(float deltaX, float deltaY)
{
    RotateAroundVertex(deltaX * mMouseSensitivity, deltaY * mMouseSensitivity, mFocusVertex);

    UpdateUpVector();

    mView.setToIdentity();
    mView.lookAt(mPosition, mFocusVertex, mUp);
}

void Camera::MousePan(float deltaX, float deltaY)
{
    // If pitch is positive (we are looking up), then flip our vertical delta.
    if (mDirection.y() > 0.f)
        deltaY *= -1;

    // Apply Sensitivity
    deltaX *= mMouseSensitivity;
    deltaY *= mMouseSensitivity;

    // Take direction/yaw into account to ensure correct horizontal x and vertical
    // y motion of the scenes x and z axes with respect to canvas screen.
    // This allows mouse panning to stay consistent regardless of camera/world rotation.
    auto yaw = std::atan2(mDirection.z(), mDirection.x());
    float delta_x = std::sin(yaw) * deltaX - std::cos(yaw) * deltaY;
    float delta_z = std::cos(yaw) * deltaX + std::sin(yaw) * deltaY;

    mPosition[0] += delta_x;
    mPosition[2] -= delta_z;

    mFocusVertex[0] += delta_x;
    mFocusVertex[2] -= delta_z;

    mView.setToIdentity();
    mView.lookAt(mPosition, mPosition + mDirection, mUp);
}

void Camera::MouseVPan(float /*deltaX*/, float deltaY)
{
    deltaY *= mMouseSensitivity; // Apply Sensitivity

    auto pitch = std::asin(mDirection.y());
    float delta_y = std::cos(pitch) * deltaY;

    mPosition[1] -= delta_y;

    mFocusVertex[1] -= delta_y;

    mView.setToIdentity();
    mView.lookAt(mPosition, mPosition + mDirection, mUp);
}

void Camera::MouseZoom(float delta)
{
    int sign = delta / std::abs(delta);
    float prev_fov = mFov;

    float new_fov = prev_fov - sign * mZoomSensitivity;

    if (new_fov > 1.f && new_fov < 90.f)
    {
        SetFov(new_fov);
    }
}

std::shared_ptr<Entity>
Camera::MousePick(float posX, float posY,
                  const std::vector<std::shared_ptr<Entity>> &entities,
                  float width, float height)
{
    // Camera ray
    // Mouse position in clip space (Converted from screen space)
    QVector3D mouse_pos(posX / width  * 2.f - 1.f,
                        posY / height * 2.f - 1.f,
                        0.f); // Near plane
    QVector3D ray_end = (mProjection * mView).inverted() * mouse_pos;
    // ray_end is in the near plane position. Therefore, get the direction
    // of the Ray and then set its distance to 10000 units
    math::Ray ray(mPosition, (ray_end - mPosition).normalized(), 10000.f);

    std::vector<math::HitResult> hits;
    for (const auto& ent : entities)
    {
        if (!ent->GetRenderModel())
            continue;

        math::HitResult hit = ray.Trace(ent);

        if (hit.mEntity)
        {
            hits.push_back(std::move(hit));
        }
    }
    std::sort(hits.begin(), hits.end(),
              [](const math::HitResult& a, const math::HitResult& b) { return a.mDistance < b.mDistance; });

    if (hits.empty())
        return nullptr;
    else
        return hits.front().mEntity;
}

/** @brief Rotates camera by delta az/el around vertex */
void Camera::RotateAroundVertex(const float deltaAz, const float deltaEl, const QVector3D& vertex)
{
    mPosition -= vertex;
    auto radius = mPosition.length();

    auto azimuth = std::atan2(mPosition.z(), mPosition.x());
    auto elevation = std::asin((mPosition.y()) / radius);

    azimuth += deltaAz;
    elevation += deltaEl;

    if (elevation > 90.f * math::constants::deg_to_rad)
        elevation = 89.9f * math::constants::deg_to_rad;
    else if (elevation < -90.f * math::constants::deg_to_rad)
        elevation = -89.9f * math::constants::deg_to_rad;

    mPosition[0] = radius * std::cos(azimuth) * std::cos(elevation);
    mPosition[1] = radius * std::sin(elevation);
    mPosition[2] = radius * std::sin(azimuth) * std::cos(elevation);
    mPosition += vertex;

    mDirection = vertex - mPosition;
    mDirection.normalize();
}

void Camera::UpdateUpVector()
{
    auto right_vector = QVector3D::crossProduct(mDirection, mWorldUp).normalized();
    mUp = QVector3D::crossProduct(right_vector, mDirection);
}

void Camera::UpdateView()
{
    mView.setToIdentity();
    mView.lookAt(mPosition, mPosition + mDirection, mUp);
}

void Camera::UpdateProjection()
{
    mProjection.setToIdentity();
    mProjection.perspective(mFov, mAspect, mNear, mFar);
}
}

