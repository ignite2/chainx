#include <glad/glad.h>
#include "GLState.h"

namespace ChainX
{
GLState::GLState()
{
}

void GLState::EnableDepthTest(bool enable)
{
    mDepthTest = enable;
    if (mDepthTest) glEnable(GL_DEPTH_TEST);
    else            glDisable(GL_DEPTH_TEST);
}

void GLState::EnableCullFace(bool enable)
{
    mCullFace = enable;
    if (mCullFace)
    {
        glEnable(GL_CULL_FACE);
        // Always cull the back for for ChainX renderer
        glCullFace(GL_BACK);
    }
    else
    {
        glDisable(GL_CULL_FACE);
    }
}

void GLState::EnableStencilTest(bool enable)
{
    mStencilTest = enable;
    if (mStencilTest)
    {
        glEnable(GL_STENCIL_TEST);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    }
    else
    {
        glDisable(GL_STENCIL_TEST);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    }
}

void GLState::SetDepthFunction(GLenum depthFunc)
{
    mDepthFunc = depthFunc;
    glDepthFunc(mDepthFunc);
}

void GLState::SetFrontFaceWindOrder(GLenum windOrder)
{
    mWindOrder = windOrder;
    glFrontFace(mWindOrder);
}

void GLState::SetPolygonMode(GLenum mode)
{
    mPolygonMode = mode;
    glPolygonMode(GL_FRONT_AND_BACK, mPolygonMode);
}

void GLState::WriteToStencil(bool write)
{
    if (write) mStencilWriteMask = 0xFF;
    else       mStencilWriteMask = 0x00;
    glStencilMask(mStencilWriteMask);
}

void GLState::SetStencilFunction(GLenum stencilFunc, GLint ref, GLuint mask)
{
    mStencilFunc = stencilFunc;
    mStencilRef  = ref;
    mStencilMask = mask;
    glStencilFunc(mStencilFunc, mStencilRef, mStencilMask);
}

void GLState::ResetStencil()
{
    // Reset stencil to default behavior
    glStencilFunc(GL_ALWAYS, 0, 0xFF);
    glStencilMask(0xFF);
    mStencilFunc      = GL_ALWAYS;
    mStencilRef       = 0;
    mStencilMask      = 0xFF;
    mStencilWriteMask = 0xFF;
}

void GLState::ApplyState()
{
    EnableDepthTest(mDepthTest);
    EnableCullFace(mCullFace);

    glDepthFunc(mDepthFunc);
    glFrontFace(mWindOrder);

    // Stencil state
    EnableStencilTest(mStencilTest);
    ResetStencil();
}
}

