//#include "glad/glad.h"
#include "Mesh.h"

#include <cmath>
#include <QOpenGLShaderProgram>


namespace ChainX
{
namespace
{
struct vec3_hash
{
    std::size_t operator()(const QVector3D& vec) const
    {
        auto h1 = std::hash<float>{}(vec.x());
        auto h2 = std::hash<float>{}(vec.y());
        auto h3 = std::hash<float>{}(vec.z());
        return h1 ^ ((h2 << 1) ^ (h3 << 3));
    }
};

QVector3D CalcTriangleNormal(const QVector3D& vert1, const QVector3D& vert2,
                             const QVector3D& vert3)
{
    auto edge1 = (vert2 - vert1).normalized();
    auto edge2 = (vert3 - vert1).normalized();
    return QVector3D::crossProduct(edge1, edge2);
}

float CalcAngle(const QVector3D& vec1, const QVector3D& vec2)
{
    return std::acos(
            QVector3D::dotProduct(vec1, vec2)
                / (vec1.length() * vec2.length()) );
}

void AddToVertexNormalMap(std::unordered_map<QVector3D, QVector3D, vec3_hash>& vertexNormalMap,
                          const QVector3D& vert, const QVector3D& norm)
{
    auto it = vertexNormalMap.find(vert);
    if (it == vertexNormalMap.end()) vertexNormalMap.emplace(vert, norm);
    else                             it->second += norm;
};

std::vector<QVector3D> CalcNormals(const std::vector<QVector3D>& vertices)
{
    std::vector<QVector3D> normals(vertices.size(), {0.f,0.f,0.f});
    for (auto i = 0u; i < vertices.size(); i+=3)
    {
        QVector3D v1 = vertices[i];
        QVector3D v2 = vertices[i+1];
        QVector3D v3 = vertices[i+2];

        QVector3D normal = CalcTriangleNormal(v1, v2, v3);
        normals[i]   = normal;
        normals[i+1] = normal;
        normals[i+2] = normal;
    }
    return normals;
}

std::vector<QVector3D> CalcNormals(const std::vector<QVector3D>& vertices,
                                   const std::vector<std::uint32_t>& indices)
{
    // Accumulate normals, then average them out, per index
    std::vector<QVector3D> normals(vertices.size(), {0.f,0.f,0.f});
    for (auto i = 0u; i < indices.size(); i+=3)
    {
        QVector3D v1 = vertices[indices[i]];
        QVector3D v2 = vertices[indices[i+1]];
        QVector3D v3 = vertices[indices[i+2]];
        QVector3D normal = CalcTriangleNormal(v1, v2, v3);
        normals[indices[i]]   += normal;
        normals[indices[i+1]] += normal;
        normals[indices[i+2]] += normal;
    }
    for (auto& normal : normals)
    {
        normal.normalize();
    }
    return normals;
}

std::vector<QVector3D> CalcSmoothNormals(const std::vector<QVector3D>& vertices)
{
    std::unordered_map<QVector3D, QVector3D, vec3_hash> vertex_normals;
    // Accumulate angle-weighted normals per vertex
    std::vector<QVector3D> normals(vertices.size(), {0.f,0.f,0.f});
    for (auto i = 0u; i < vertices.size(); i+=3)
    {
        QVector3D v1 = vertices[i];
        QVector3D v2 = vertices[i+1];
        QVector3D v3 = vertices[i+2];

        // Angle of vertex and its neighboor vertices
        float a1 = CalcAngle(v2 - v1, v3 - v1);
        float a2 = CalcAngle(v3 - v2, v1 - v2);
        float a3 = CalcAngle(v1 - v3, v2 - v3);

        QVector3D normal = CalcTriangleNormal(v1, v2, v3).normalized();
        AddToVertexNormalMap(vertex_normals, v1, normal*a1);
        AddToVertexNormalMap(vertex_normals, v2, normal*a2);
        AddToVertexNormalMap(vertex_normals, v3, normal*a3);
    }
    // Average normals out per vertex
    for (auto i = 0u; i < vertices.size(); i+=3)
    {
        QVector3D v1 = vertices[i];
        QVector3D v2 = vertices[i+1];
        QVector3D v3 = vertices[i+2];

        QVector3D n1 = vertex_normals.at(v1);
        QVector3D n2 = vertex_normals.at(v2);
        QVector3D n3 = vertex_normals.at(v3);

        normals[i]   = n1.normalized();
        normals[i+1] = n2.normalized();
        normals[i+2] = n3.normalized();
    }
    return normals;
}

std::vector<QVector3D> CalcSmoothNormals(const std::vector<QVector3D>& vertices,
                                         const std::vector<std::uint32_t>& indices)
{
    std::unordered_map<QVector3D, QVector3D, vec3_hash> vertex_normals;
    // Accumulate angle-weighted normals per vertex
    std::vector<QVector3D> normals(vertices.size(), {0.f,0.f,0.f});
    for (auto i = 0u; i < indices.size(); i+=3)
    {
        QVector3D v1 = vertices[indices[i]];
        QVector3D v2 = vertices[indices[i+1]];
        QVector3D v3 = vertices[indices[i+2]];

        // Angle of vertex between neighboor vertices
        float a1 = CalcAngle(v2 - v1, v3 - v1);
        float a2 = CalcAngle(v3 - v2, v1 - v2);
        float a3 = CalcAngle(v1 - v3, v2 - v3);

        QVector3D normal = CalcTriangleNormal(v1, v2, v3).normalized();
        AddToVertexNormalMap(vertex_normals, v1, normal*a1);
        AddToVertexNormalMap(vertex_normals, v2, normal*a2);
        AddToVertexNormalMap(vertex_normals, v3, normal*a3);
    }
    // Average normals out and save to correct index
    for (auto i = 0u; i < indices.size(); i+=3)
    {
        QVector3D v1 = vertices[indices[i]];
        QVector3D v2 = vertices[indices[i+1]];
        QVector3D v3 = vertices[indices[i+2]];

        QVector3D n1 = vertex_normals.at(v1);
        QVector3D n2 = vertex_normals.at(v2);
        QVector3D n3 = vertex_normals.at(v3);

        normals[indices[i]]   = n1.normalized();
        normals[indices[i+1]] = n2.normalized();
        normals[indices[i+2]] = n3.normalized();
    }
    return normals;
}
} // anonymous namespace


Mesh::Mesh()
    :
      mVao(new QOpenGLVertexArrayObject),
      mVbo(QOpenGLBuffer::VertexBuffer),
      mEbo(QOpenGLBuffer::IndexBuffer),
      mTopology(GL_TRIANGLES)
{
}

Mesh::Mesh(std::vector<QVector3D> vertices)
    :
      mVao(new QOpenGLVertexArrayObject),
      mVbo(QOpenGLBuffer::VertexBuffer),
      mEbo(QOpenGLBuffer::IndexBuffer),
      mVertices(std::move(vertices)),
      mTopology(GL_TRIANGLES)
{
}

Mesh::Mesh(std::vector<QVector3D> vertices, std::vector<std::uint32_t> indices)
    :
      mVao(new QOpenGLVertexArrayObject),
      mVbo(QOpenGLBuffer::VertexBuffer),
      mEbo(QOpenGLBuffer::IndexBuffer),
      mVertices(std::move(vertices)),
      mIndices(std::move(indices)),
      mTopology(GL_TRIANGLES)
{
}

Mesh::Mesh(std::vector<QVector3D> vertices, std::vector<QVector3D> normals)
    :
      mVao(new QOpenGLVertexArrayObject),
      mVbo(QOpenGLBuffer::VertexBuffer),
      mVertices(std::move(vertices)),
      mNormals(std::move(normals)),
      mTopology(GL_TRIANGLES)
{
}

Mesh::Mesh(std::vector<QVector3D> vertices, std::vector<QVector3D> normals,
           std::vector<std::uint32_t> indices)
    :
      mVao(new QOpenGLVertexArrayObject),
      mVbo(QOpenGLBuffer::VertexBuffer),
      mEbo(QOpenGLBuffer::IndexBuffer),
      mVertices(std::move(vertices)),
      mNormals(std::move(normals)),
      mIndices(std::move(indices)),
      mTopology(GL_TRIANGLES)
{
}

Mesh::Mesh(std::vector<QVector3D> vertices, std::vector<QVector3D> normals,
           std::vector<QVector2D> uvCoords, std::vector<std::uint32_t> indices)
    :
      mVao(new QOpenGLVertexArrayObject),
      mVbo(QOpenGLBuffer::VertexBuffer),
      mEbo(QOpenGLBuffer::IndexBuffer),
      mVertices(std::move(vertices)),
      mNormals(std::move(normals)),
      mUV(std::move(uvCoords)),
      mIndices(std::move(indices)),
      mTopology(GL_TRIANGLES)
{
}

void Mesh::SetTopology(const Topology mode)
{
    if (mode == Topology::LINES)
        mTopology = GL_LINES;
    else if (mode == Topology::TRIANGLES)
        mTopology = GL_TRIANGLES;
    else if (mode == Topology::TRIANGLE_STRIP)
        mTopology = GL_TRIANGLE_STRIP;
}

GLenum Mesh::GLTopology() const
{
    return mTopology;
}

void Mesh::CalculateNormals()
{
    if (!mNormals.empty())
        return;

    if (mIndices.empty())
    {
        mNormals = CalcNormals(mVertices);
    }
    else
    {
        mNormals = CalcNormals(mVertices, mIndices);
    }
}

void Mesh::CalculateSmoothNormals()
{
    if (mIndices.empty())
    {
        mNormals = CalcSmoothNormals(mVertices);
    }
    else
    {
        mNormals = CalcSmoothNormals(mVertices, mIndices);
    }
}

void Mesh::Finalize(QOpenGLShaderProgram* shader)
{
    if (!mVao->isCreated())
       mVao->create();
    mVao->bind();

    if (!mVbo.isCreated())
        mVbo.create();
    mVbo.bind();
    mVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);

    // Fill data buffer
    std::vector<float> data;
    for (auto i = 0u; i < mVertices.size(); ++i)
    {
        data.push_back(mVertices[i].x());
        data.push_back(mVertices[i].y());
        data.push_back(mVertices[i].z());

        if (mNormals.size())
        {
            data.push_back(mNormals[i].x());
            data.push_back(mNormals[i].y());
            data.push_back(mNormals[i].z());
        }
        if (mUV.size())
        {
            data.push_back(mUV[i].x());
            data.push_back(mUV[i].y());
        }
    }
    auto data_size = static_cast<int>(data.size());
    mVbo.allocate(data.data(), data_size * sizeof(float));

    // Fill index buffer
    if (mIndices.size())
    {
        auto indices_size = static_cast<int>(mIndices.size());
        if (!mEbo.isCreated())
            mEbo.create();
        mEbo.bind();
        mEbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
        mEbo.allocate(mIndices.data(), indices_size * sizeof(std::uint32_t));
    }

    // Calculate stride for non-empty vertex attribute arrays
    auto stride = 3 * sizeof(float);
    if (mNormals.size())
        stride += 3 * sizeof(float);
    if (mUV.size())
        stride += 2 * sizeof(float);

    // Fill vertex attribute arrays
    auto offset = 0;
    shader->enableAttributeArray("position");
    shader->setAttributeBuffer("position", GL_FLOAT, offset, 3, stride);
    offset += 3 * sizeof(float);

    if (mNormals.size())
    {
        shader->enableAttributeArray("normal");
        shader->setAttributeBuffer("normal", GL_FLOAT, offset, 3, stride);
        offset += 3 * sizeof(float);
    }
    if (mUV.size())
    {
        shader->enableAttributeArray("uvCoord");
        shader->setAttributeBuffer("uvCoord", GL_FLOAT, offset, 2, stride);
    }

    // Unbind buffers and vao
    mVao->release();
    mVbo.release();
    if (mIndices.size())
        mEbo.release();

    //    glEnableVertexAttribArray(0);
    //    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
}

std::uint32_t Mesh::NumTriangles()
{
    auto num = 0u;

    if (mTopology == GL_TRIANGLES)
    {
        if (mIndices.size())
        {
            num = static_cast<unsigned int>(mIndices.size()) / 3u;
        }
        else
        {
            num = static_cast<unsigned int>(mVertices.size()) / 3u;
        }
    }
    else if (mTopology == GL_TRIANGLE_STRIP)
    {
        if (mIndices.size())
        {
            num = static_cast<unsigned int>(mIndices.size()) - 2u;
        }
        else
        {
            num = static_cast<unsigned int>(mVertices.size()) - 2u;
        }
    }

    return num;
}
}
