#include "Texture.h"
#include <stb_image/stb_image.h>

namespace ChainX
{
Texture::Texture(std::string filename, GLenum textureTarget)
    : mFilename(filename),
      mTexTarget(textureTarget)
{
}

void Texture::Generate(GLenum colorFormat, bool useMipmap)
{
    int width;
    int height;
    int channels;
    std::uint8_t* data = stbi_load(mFilename.c_str(), &width, &height, &channels, 0);

    if (data)
    {
        GLenum pix_format = GL_RGBA;
        if (channels == 1)
            pix_format = GL_RED;
        else if (channels == 3)
            pix_format = GL_RGB;

        glGenTextures(1, &mTexID);
        glBindTexture(mTexTarget, mTexID);

        if (mTexTarget == GL_TEXTURE_2D)
        {
            auto detail_lvl = 0;
            auto border = 0;
            glTexImage2D(mTexTarget, detail_lvl, colorFormat, width, height,
                         border, pix_format, GL_UNSIGNED_BYTE, data);
            glTexParameteri(mTexTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(mTexTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(mTexTarget, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(mTexTarget, GL_TEXTURE_WRAP_T, GL_REPEAT);
            // Add GL_TEXTURE_BORDER_COLOR?

            if (useMipmap)
            {
                glGenerateMipmap(mTexTarget);
                glTexParameteri(mTexTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
            }
        }

        stbi_image_free(data);
    }
}

void Texture::Bind(GLenum textureUnit)
{
    glActiveTexture(textureUnit);
    glBindTexture(mTexTarget, mTexID);
}

void Texture::Unbind()
{
    glBindTexture(mTexTarget, 0);
}
}
