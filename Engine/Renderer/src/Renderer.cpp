#include <glad/glad.h>

#include "Renderer.h"
#include "Camera.h"
#include "Entity.h"
#include "RenderModel.h"
#include "Transform.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "Texture.h"

namespace ChainX
{
namespace
{
// Renderer-specific constants
constexpr int MAX_PT_LIGHTS = 8;
constexpr int MAX_DIR_LIGHTS = 2;
// Type sizes for Uniform Buffer Objects for GLSL
constexpr unsigned int size_mat4x4 = 64;
constexpr unsigned int size_vec4   = 16;
constexpr unsigned int size_vec2   = 8;
constexpr unsigned int size_scalar = 4;
}


Renderer::Renderer()
{
}

void Renderer::Init()
{
    mShaderLibrary.Init();

    glClearColor(.8f, .8f, .8f, .8f);
    glClearDepth(1.f);

    glEnable(GL_MULTISAMPLE);

    mGLState.EnableDepthTest(true);
    mGLState.EnableCullFace(false);
    mGLState.SetFrontFaceWindOrder(GL_CCW);
    ShowMeshOutlines(true);

    // UBO setup
    glGenBuffers(1, &mMatricesUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, mMatricesUBO);
    glBufferData(GL_UNIFORM_BUFFER, 2*size_mat4x4, nullptr, GL_STATIC_DRAW);
    glBindBufferRange(GL_UNIFORM_BUFFER, 0, mMatricesUBO, 0, 2*size_mat4x4);

    // See UpdateUBOs for struct layout
    // vec4             16
    // DirectionalLight 64 * 2 (Max Num Dir   Lights)
    // PointLight       48 * 8 (Max Num Point Lights)
    // scalar (int)     4
    // scalar (int)     4
    constexpr GLsizeiptr buf_size =
            size_vec4 + (sizeof_DirLight*2) + (sizeof_PointLight*8) + (2*size_scalar);
    glGenBuffers(1, &mSceneUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, mSceneUBO);
    glBufferData(GL_UNIFORM_BUFFER, buf_size, nullptr, GL_STATIC_DRAW);
    glBindBufferRange(GL_UNIFORM_BUFFER, 1, mSceneUBO, 0, buf_size);

    auto grid_shader = mShaderLibrary.GetShader(ShaderType::GPU_UNIFORM_COLOR);
    mGrid.SetShader(grid_shader);
}

void Renderer::SetRenderItems(std::vector<std::shared_ptr<Entity>> entities, Camera* camera)
{
    mCamera = camera;

    mRenderEntities.clear();
    mDirLightEntities.clear();
    mPointLightEntities.clear();
    mNumPointLights = 0;
    mNumDirLights = 0;

    for (auto entity : entities)
    {
        RenderModel* render_model = entity->GetRenderModel();
        if (render_model)
        {
            if (render_model->GetMesh() && render_model->GetMaterial())
            {
                mRenderEntities.push_back(entity);
            }
        }

        if (entity->GetPointLight() != nullptr && mNumPointLights < MAX_PT_LIGHTS)
        {
            mPointLightEntities.push_back(entity);
            ++mNumPointLights;
        }
        else if (entity->GetDirLight() && mNumDirLights < MAX_DIR_LIGHTS)
        {
            mDirLightEntities.push_back(entity);
            ++mNumDirLights;
        }
    }
}

void Renderer::SetRenderSize(unsigned int width, unsigned int height)
{
    mWidth = width;
    mHeight = height;
}

void Renderer::SetHandleToEntity(const std::shared_ptr<Entity>& entity)
{
    if (!entity || !entity->GetRenderModel())
    {
        mEntityHandle.SetSelectedEntity(nullptr);
        return;
    }
    else
    {
        mEntityHandle.SetSelectedEntity(entity);

        // TODO: This can be cleaned for getting the shader
        mEntityHandle.OutlineMesh()->Finalize(
                mShaderLibrary.GetShader(ShaderType::GPU_OUTLINE).get());
    }
    // TODO: Don't compute outline mesh if stencil is disabled
    // TODO: Snap to handle position
}

void Renderer::ShowGrid(bool show)
{
    mDrawGrid = show;
}

void Renderer::ShowMeshOutlines(bool show)
{
    mGLState.EnableStencilTest(show);
}

void Renderer::RenderFrame()
{
    if (!mCamera)
        return;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);


    UpdateUniformBuffers();

    // 1. First Render Pass of models
    DrawModels();

    // 2. Draw PostProcess visuals
    DrawOutlines();
    DrawTransformHandle();

    if (mDrawGrid)
    {
        mGrid.Shader()->bind();
        mGrid.SetParameters();
        RenderMesh(mGrid.MeshRef());
        mGrid.Shader()->release();
    }
}

void Renderer::DrawModels()
{
    auto sel_entity = mEntityHandle.SelectedEntity();

    for (const auto& entity : mRenderEntities)
    {
        // Check if stencil buffer should be updated
        if (sel_entity && sel_entity->GetId() == entity->GetId())
        {
            mGLState.SetStencilFunction(GL_ALWAYS, 1, 0xFF);
            mGLState.WriteToStencil(true);
        }

        RenderCommand cmd;
        cmd.mesh = entity->GetRenderModel()->GetMesh();
        cmd.material = entity->GetRenderModel()->GetMaterial();
        cmd.model_transform = entity->GetTransform()->GetMatrix();

        ExecRenderCommand(cmd);

        if (sel_entity && sel_entity->GetId() == entity->GetId())
        {
            mGLState.WriteToStencil(false); // No further writing to Stencil buffer
        }
    }
}

void Renderer::DrawOutlines()
{
    // No Outlines Render Pass if no stencil buffer
    if (!mGLState.IsStencilEnable())
        return;

    auto sel_entity = mEntityHandle.SelectedEntity();
    if (sel_entity == nullptr)
        return;

    // Ensure outline doesn't draw over mesh
    mGLState.SetStencilFunction(GL_NOTEQUAL, 1, 0xFF);
    mGLState.WriteToStencil(false);
    mGLState.EnableDepthTest(false);

    // Use outline shader
    auto outline_shader = mShaderLibrary.GetShader(ShaderType::GPU_OUTLINE);
    outline_shader->bind();

    Mesh* outline_mesh = mEntityHandle.OutlineMesh();
    QMatrix4x4 model = sel_entity->GetTransform()->GetMatrix();
    QVector3D scale = sel_entity->GetTransform()->WorldScale();
    outline_shader->setUniformValue("model", model);
    outline_shader->setUniformValue("scale", scale);
    RenderMesh(*outline_mesh);

    outline_shader->release();

    mGLState.ResetStencil();
    mGLState.EnableDepthTest(true);
}

void Renderer::DrawTransformHandle()
{
    std::shared_ptr<Entity> entity = mEntityHandle.SelectedEntity();
    if (entity == nullptr)
        return;

    // TODO: Add Blending capabilities

    TransformHandle* handle = mEntityHandle.GetTransformHandle();
    // TODO: Update handle transform
    handle->Update(entity.get());

    auto shader = mShaderLibrary.GetShader(ShaderType::GPU_UNIFORM_COLOR);
    handle->SetShader(shader); // TODO: Only do this once when initializing transform handle
    shader->bind();

    // TODO: Clean
    // TODO: Draw the axes attach link here as well
    const Mesh& handle_mesh = handle->GetHandleMesh(TransformAxis::X);
    // X axis
    QMatrix4x4 model = handle->GetTransform(TransformAxis::X);
    shader->setUniformValue("model", model);
    shader->setUniformValue("color", handle->GetColor(TransformAxis::X));
    RenderMesh(handle_mesh);
    // Y axis
    model = handle->GetTransform(TransformAxis::Y);
    shader->setUniformValue("model", model);
    shader->setUniformValue("color", handle->GetColor(TransformAxis::Y));
    RenderMesh(handle_mesh);
    // Z axis
    model = handle->GetTransform(TransformAxis::Z);
    shader->setUniformValue("model", model);
    shader->setUniformValue("color", handle->GetColor(TransformAxis::Z));
    RenderMesh(handle_mesh);

    shader->release();
}

void Renderer::ExecRenderCommand(const RenderCommand& cmd)
{
    cmd.material->Shader()->bind();
    // Set material specific parameters
    cmd.material->SetParameters();

    cmd.material->Shader()->setUniformValue("model", cmd.model_transform);

    RenderMesh(*cmd.mesh);

    cmd.material->Shader()->release();
}

void Renderer::RenderMesh(const Mesh& mesh)
{
    mesh.VertexArrayBuf()->bind();

    if (mesh.Indices().size())
        glDrawElements(mesh.GLTopology(), int(mesh.Indices().size()), GL_UNSIGNED_INT, 0);
    else
        glDrawArrays(mesh.GLTopology(), 0, int(mesh.Vertices().size()));

    mesh.VertexArrayBuf()->release();
}

void Renderer::UpdateUniformBuffers()
{
    // == Matrices UBO
    glBindBuffer(GL_UNIFORM_BUFFER, mMatricesUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0,           size_mat4x4, mCamera->ViewMatrix().data());
    glBufferSubData(GL_UNIFORM_BUFFER, size_mat4x4, size_mat4x4, mCamera->ProjMatrix().data());
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    // == Scene UBO
    // Camera View Position
    QVector4D cam_pos = mCamera->Position();
    glBindBuffer(GL_UNIFORM_BUFFER, mSceneUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, size_vec4, &cam_pos[0]);
    // DirectionalLights
    unsigned int stride = sizeof_DirLight;
    unsigned int start = size_vec4;
    for (auto i = 0u; i < mDirLightEntities.size(); ++i)
    {
        DirectionalLight* dlight = mDirLightEntities[i]->GetDirLight();
        auto offset = start + i*stride;
        glBufferSubData(GL_UNIFORM_BUFFER, offset,                       size_vec4, &dlight->direction[0]);
        glBufferSubData(GL_UNIFORM_BUFFER, offset + offsets_DirLight[1], size_vec4, &dlight->ambient[0]);
        glBufferSubData(GL_UNIFORM_BUFFER, offset + offsets_DirLight[2], size_vec4, &dlight->diffuse[0]);
        glBufferSubData(GL_UNIFORM_BUFFER, offset + offsets_DirLight[3], size_vec4, &dlight->specular[0]);
    }
    // PointLights
    stride = sizeof_PointLight;
    start = size_vec4 + (sizeof_DirLight*2);
    for (auto i = 0u; i < mPointLightEntities.size(); ++i)
    {
        // Always use Transform position for point lights, so update point light pos from transform
        PointLight* ptlight = mPointLightEntities[i]->GetPointLight();
        ptlight->position   = mPointLightEntities[i]->GetTransform()->Position();
        auto offset = start + i*stride;
        glBufferSubData(GL_UNIFORM_BUFFER, offset,                         size_vec4, &ptlight->position[0]);
        glBufferSubData(GL_UNIFORM_BUFFER, offset + offsets_PointLight[1], size_vec4, &ptlight->color[0]);
        glBufferSubData(GL_UNIFORM_BUFFER, offset + offsets_PointLight[2], size_scalar, &ptlight->constant);
        glBufferSubData(GL_UNIFORM_BUFFER, offset + offsets_PointLight[3], size_scalar, &ptlight->linear);
        glBufferSubData(GL_UNIFORM_BUFFER, offset + offsets_PointLight[4], size_scalar, &ptlight->quadratic);
        // padding 4 bytes (Need to pad struct multiple of 16)
    }
    // Camera Pos + PointLights * 8 + DirectionalLights * 2
    start = size_vec4 + (sizeof_DirLight*2) + (sizeof_PointLight*8);
    glBufferSubData(GL_UNIFORM_BUFFER, start,               size_scalar, &mNumDirLights);
    glBufferSubData(GL_UNIFORM_BUFFER, start + size_scalar, size_scalar, &mNumPointLights);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}
}

