#pragma once

#include "BoundingBox.h"
#include "HitResult.h"
#include <QVector3D>

namespace ChainX
{
class Entity;

namespace math
{
/**
 * @brief A Ray or Line Trace that represents an invisible ray from a start
 * point to an end point. Used to detect geometry between the ray's two points.
 * Bounding boxes are used for geometry calculations.
 */
class Ray
{
public:
    Ray(const QVector3D& start, const QVector3D& end);
    Ray(const QVector3D& start, const QVector3D& direction, float length);

    HitResult Trace(const std::shared_ptr<Entity>& entity);

private:
    QVector3D mStart;
    QVector3D mEnd;
    QVector3D mDirection;
};
}
}


