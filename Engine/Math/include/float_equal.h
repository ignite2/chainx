#pragma once

#include <algorithm>
#include <cmath>
#include <limits>
#include <type_traits>
#include <typeinfo>


namespace math
{
template <typename T, typename U>
struct float_common : std::conditional<(sizeof(T) > sizeof(U)), T, U>
{
};

struct compare_epsilon
{
    template<typename T>
    typename std::enable_if<std::is_floating_point<T>::value, bool>::type
    operator()(const T f1, const T f2) const
    {
        return std::fabs(f1 - f2) <=
                std::min(std::fabs(f1), std::fabs(f2)) * std::numeric_limits<T>::epsilon();
    }
};

template<typename T, typename U, typename FloatCompare = compare_epsilon>
bool float_equal(const T a, const U b, FloatCompare compare = compare_epsilon())
{
    using float_type = typename float_common<T, U>::type;
    auto f1 = static_cast<float_type>(a);
    auto f2 = static_cast<float_type>(b);

    return compare(f1, f2);
}
}
