#pragma once

#include <memory>
#include <QVector3D>

namespace ChainX
{
class Entity;

namespace math
{
struct HitResult
{
    std::shared_ptr<Entity> mEntity;
    QVector3D mHitPosition;
    float mDistance;
};
}
}

