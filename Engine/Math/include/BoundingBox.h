#pragma once

#include <QMatrix4x4>
#include <vector>

namespace ChainX
{
namespace math
{
/**
 * @brief Bounding box class that also provides functionality to obtain
 * Axis Aligned Bounding Boxes
 */
class BoundingBox
{
public:
    BoundingBox() = default;
    BoundingBox(const QVector3D& min, const QVector3D& max);
    BoundingBox(const std::vector<QVector3D>& vertices);

    /**
     * @brief Get an Axis Aligned Bounding Box (AABB) from this bounding box and
     * the transformation matrix parameter
     */
    BoundingBox GetAABB(const QMatrix4x4& transform);

    /// Properties
    const QVector3D& Min() const { return mMin; }
    const QVector3D& Max() const { return mMax; }

    QVector3D GetCenter() const { return (mMax + mMin) * .5f; }
    QVector3D GetExtent() const { return mMax - mMin; }

private:
    QVector3D mMin;
    QVector3D mMax;
};
}
}


