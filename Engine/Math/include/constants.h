#pragma once

#include <cmath>

namespace ChainX
{
namespace math::constants
{
inline constexpr float pi = 3.1415927f;
inline constexpr float pi_2    = 2 * pi;
inline constexpr float pi_half = pi / 2;

inline constexpr float deg_to_rad = pi / 180.0f;
inline constexpr float rad_to_deg = 180.f / pi;
}
}
