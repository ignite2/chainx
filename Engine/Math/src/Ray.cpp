#include "Ray.h"
#include "Entity.h"
#include "RenderModel.h"
#include <cmath>

namespace ChainX::math
{
namespace
{
// Updates hitDist if ray trace intersects the bounding box at the given distance.
// Helper functions for X, Y, and Z intersection distances
void CheckHitDistanceX(float& hitDist, float distance, const BoundingBox& box,
                       const QVector3D& rayStart, const QVector3D& rayDir)
{
    if (distance < hitDist)
    {
        QVector3D point = rayStart + distance * rayDir;
        if (point.y() >= box.Min().y() && point.y() <= box.Max().y() &&
            point.z() >= box.Min().z() && point.z() <= box.Max().z())
        {
            hitDist = distance;
        }
    }
}
void CheckHitDistanceY(float& hitDist, float distance, const BoundingBox& box,
                       const QVector3D& rayStart, const QVector3D& rayDir)
{
    if (distance < hitDist)
    {
        QVector3D point = rayStart + distance * rayDir;
        if (point.x() >= box.Min().x() && point.x() <= box.Max().x() &&
            point.z() >= box.Min().z() && point.z() <= box.Max().z())
        {
            hitDist = distance;
        }
    }
}
void CheckHitDistanceZ(float& hitDist, float distance, const BoundingBox& box,
                       const QVector3D& rayStart, const QVector3D& rayDir)
{
    if (distance < hitDist)
    {
        QVector3D point = rayStart + distance * rayDir;
        if (point.x() >= box.Min().x() && point.x() <= box.Max().x() &&
            point.y() >= box.Min().y() && point.y() <= box.Max().y())
        {
            hitDist = distance;
        }
    }
}

bool IsBetween(float pos, float start, float end)
{
    return (start < pos && pos < end) || (start > pos && pos > end);
}
} // end anonymous namespace

Ray::Ray(const QVector3D& start, const QVector3D& end)
    :
      mStart(start), mEnd(end)
{
    mDirection = (mEnd - mStart).normalized();
}

Ray::Ray(const QVector3D& start, const QVector3D& direction, float length)
    :
      mStart(start), mDirection(direction)
{
    mEnd = mStart + length*direction;
}

// TODO: Use Ray start -> end or start and direction?
HitResult Ray::Trace(const std::shared_ptr<Entity>& entity)
{
    float hit_distance = std::numeric_limits<float>::max();

    const BoundingBox& box = entity->GetRenderModel()->GetBoundingBox();

    // X intersection
    if (IsBetween(box.Min().x(), mStart.x(), mEnd.x()))
    {
        float dist = (box.Min().x() - mStart.x()) / mDirection.x();
        CheckHitDistanceX(hit_distance, dist, box, mStart, mDirection);
    }
    if (IsBetween(box.Max().x(), mStart.x(), mEnd.x()))
    {
        float dist = (box.Max().x() - mStart.x()) / mDirection.x();
        CheckHitDistanceX(hit_distance, dist, box, mStart, mDirection);
    }
    // Y intersection
    if (IsBetween(box.Min().y(), mStart.y(), mEnd.y()))
    {
        float dist = (box.Min().y() - mStart.y()) / mDirection.y();
        CheckHitDistanceY(hit_distance, dist, box, mStart, mDirection);
    }
    if (IsBetween(box.Max().y(), mStart.y(), mEnd.y()))
    {
        float dist = (box.Max().y() - mStart.y()) / mDirection.y();
        CheckHitDistanceY(hit_distance, dist, box, mStart, mDirection);
    }
    // Z intersection
    if (IsBetween(box.Min().z(), mStart.z(), mEnd.z()))
    {
        float dist = (box.Min().z() - mStart.z()) / mDirection.z();
        CheckHitDistanceZ(hit_distance, dist, box, mStart, mDirection);
    }
    if (IsBetween(box.Max().z(), mStart.z(), mEnd.z()))
    {
        float dist = (box.Max().z() - mStart.z()) / mDirection.z();
        CheckHitDistanceZ(hit_distance, dist, box, mStart, mDirection);
    }

    if (hit_distance == std::numeric_limits<float>::max())
    {
        return HitResult{ nullptr, QVector3D(), hit_distance };
    }
    else
        return HitResult{ entity, mStart + hit_distance*mDirection, hit_distance };
}

}

