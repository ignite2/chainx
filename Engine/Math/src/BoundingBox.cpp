#include "BoundingBox.h"

#include <limits>

namespace ChainX
{
namespace math
{
BoundingBox::BoundingBox(const QVector3D& min, const QVector3D& max)
    :
      mMin(min), mMax(max)
{
}

BoundingBox::BoundingBox(const std::vector<QVector3D>& vertices)
{
    auto f_max = std::numeric_limits<float>::max();
    auto f_min = std::numeric_limits<float>::lowest();
    mMax = { f_min, f_min, f_min };
    mMin = { f_max, f_max, f_max };

    for (auto &vPos : vertices)
    {
        mMax.setX( std::max(mMax.x(), vPos.x()) );
        mMax.setY( std::max(mMax.y(), vPos.y()) );
        mMax.setZ( std::max(mMax.z(), vPos.z()) );

        mMin.setX( std::min(mMin.x(), vPos.x()) );
        mMin.setY( std::min(mMin.y(), vPos.y()) );
        mMin.setZ( std::min(mMin.z(), vPos.z()) );
    }
}

BoundingBox BoundingBox::GetAABB(const QMatrix4x4 &transform)
{
    QVector3D center_aabb = transform * GetCenter();
    QVector3D extent = GetExtent();
    QVector3D extent_aabb =
    {
        std::abs(transform(0,0)) * extent.x() + std::abs(transform(0,1)) * extent.y() + std::abs(transform(0,2)) * extent.z() ,
        std::abs(transform(1,0)) * extent.x() + std::abs(transform(1,1)) * extent.y() + std::abs(transform(1,2)) * extent.z() ,
        std::abs(transform(2,0)) * extent.x() + std::abs(transform(2,1)) * extent.y() + std::abs(transform(2,2)) * extent.z() ,
    };
    return BoundingBox(center_aabb - extent_aabb*.5f, center_aabb + extent_aabb*.5f);
}

}
}
