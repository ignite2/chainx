#include "Logger.h"

#include <spdlog/sinks/basic_file_sink.h>


namespace ChainX
{
namespace
{
}
std::shared_ptr<spdlog::logger> Logger::mCoreLogger;

void Logger::Init()
{
    auto log_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("ChainX.log", true);

    log_sink->set_pattern("[%T] [%l] %v");

    mCoreLogger = std::make_shared<spdlog::logger>("ChainX", log_sink);
    mCoreLogger->set_level(spdlog::level::info);
    mCoreLogger->flush_on(spdlog::level::info);
}

//void Logger::Info(const char* text, ...)
//{
//    char buffer[2048];
//    va_list args;
//    va_start(args, text);
//    vsnprintf(buffer, sizeof(buffer), text, args);
//    va_end(args);
//}
}
