#pragma once

#include <spdlog/logger.h>
#include <memory>

namespace ChainX
{
class Logger
{
public:
    static void Init();

    template<typename...Args>
    static void LogInfo(const char* text, Args&&... args)
    {
        mCoreLogger->info(text, std::forward<Args>(args)...);
    };

    template<typename...Args>
    static void LogWarning(const char* text, Args&&... args)
    {
        mCoreLogger->warn(text, std::forward<Args>(args)...);
    };

    template<typename...Args>
    static void LogError(const char* text, Args&&... args)
    {
        mCoreLogger->error(text, std::forward<Args>(args)...);
    };

    static std::shared_ptr<spdlog::logger> GetCoreLogger() { return mCoreLogger; }

private:
    static std::shared_ptr<spdlog::logger> mCoreLogger;
};
}

#define LOG_INFO(...)    ::ChainX::Logger::GetCoreLogger()->info(__VA_ARGS__)
#define LOG_WARN(...)    ::ChainX::Logger::GetCoreLogger()->warn(__VA_ARGS__)
#define LOG_ERROR(...)   ::ChainX::Logger::GetCoreLogger()->error(__VA_ARGS__)

