#pragma once

#include "Module.h"

#include <memory>
#include <vector>


namespace ChainX
{
class Context
{
public:
    Context() = default;

    template <typename T>
    typename std::enable_if<std::is_base_of<Module, T>::value, void>::type
    AddModule()
    {
        mModules.emplace_back(std::make_shared<T>(this));
    }

    template <typename T>
    typename std::enable_if<std::is_base_of<Module, T>::value, T>::type*
    GetModule() const
    {
        for (const auto& module : mModules)
        {
            if (module == nullptr)
                continue;
            auto module_raw = module.get();
            if (typeid(*module_raw) == typeid(T))
            {
                return static_cast<T*>(module_raw);
            }
        }
        return nullptr;
    }

private:
    std::vector<std::shared_ptr<Module>> mModules;
};
}


