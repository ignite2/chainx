#pragma once


namespace ChainX
{
class Context;

class Module
{
public:
    Module(Context* context) : mContext(context) {};
    virtual ~Module() = default;

protected:
    Context* mContext;
};
}


